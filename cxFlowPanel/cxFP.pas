// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxFP;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, cxOG, ImgList, Messages, cxLookAndFeels;

type
  TcxCustomTextFlowPanel = class(TcxCustomOfficeGizmo, IdxSkinSupport)
  private
    FCaption: TCaption;
    FContentOffset: Integer;
    FImageIndex: TImageIndex;
    FImageOffset: Integer;
    FTitle: TCaption;
    procedure DoContentUpdate;
    function HasImage: Boolean;
    function MouseOverContent: Boolean;
    procedure PaintCaption;
    procedure PaintImage;
    procedure SetCaption(const Value: TCaption);
    procedure SetConstraints;
    procedure SetContentOffset(const Value: Integer);
    procedure SetImageIndex(const Value: TImageIndex);
    procedure SetImageOffset(const Value: Integer);
    procedure SetTitle(const Value: TCaption);
  protected
    { Protected declarations }
    procedure ActionChange(Sender: TObject; CheckDefaults: Boolean); override;
    procedure DoImageListChange; override;
    procedure ImageListChange(Sender: TObject); override;
    procedure Paint; override;                          
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SETCURSOR;
  public                       
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure Loaded; override;
    procedure ResizeControl;
    property Caption: TCaption read FCaption write SetCaption;
    property ContentOffset: Integer read FContentOffset write SetContentOffset default 8;
    property ImageIndex: TImageIndex read FImageIndex write SetImageIndex default -1;
    property ImageOffset: Integer read FImageOffset write SetImageOffset default 8;
    property Title: TCaption read FTitle write SetTitle;
  end;

  TcxTextFlowPanel = class(TcxCustomTextFlowPanel)
  published
    { Published declarations }
    property Action;
    property Align;
    property Borders;
    property BorderWidth;
    property Caption;
    property ContentOffset;
    property Enabled;
    property ImageIndex;
    property ImageOffset;
    property Images;        
    property LookAndFeel;      
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDblClick;
    property OnMouseActivate;
    property OnMouseDown;   
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property PaintStyle;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu; 
    property ShowBorder;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Title;
    property Transparent;
    property Visible;
  end;

implementation

uses Windows, Graphics, Types, ActnList, Forms, cxLookAndFeelPainters;

{ TcxCustomFlowPanel }

constructor TcxCustomTextFlowPanel.Create(AOwner: TComponent);
begin
  inherited;
  FContentOffset := 8;
  FImageOffset := 8;
  FCaption := '';
  FTitle := '';
  FImageIndex := -1;
  SetConstraints;
  DoContentUpdate;
end;

procedure TcxCustomTextFlowPanel.ActionChange(Sender: TObject;
  CheckDefaults: Boolean);
begin
  //don't call inherited here, or the action will update the underlying gizmo and add a duplicate image
  if Sender is TCustomAction then
    with TCustomAction(Sender) do
    begin
      if not CheckDefaults or (Self.ImageIndex = -1) then
        Self.ImageIndex := ImageIndex;
      if not CheckDefaults or (Self.Caption = '') then
        Self.Caption := Caption;
    end;
end;

procedure TcxCustomTextFlowPanel.DoContentUpdate;
begin
  SetConstraints;
  ResizeControl;
  PaintImage;
  PaintCaption;
end;

procedure TcxCustomTextFlowPanel.DoImageListChange;
begin
  inherited;
  SetConstraints;
end;

function TcxCustomTextFlowPanel.HasImage: Boolean;
begin
  Result := Assigned(Images) and (ImageIndex >= 0) and
    (ImageIndex < Images.Count);
end;

procedure TcxCustomTextFlowPanel.ImageListChange(Sender: TObject);
begin
  inherited;
  SetConstraints;
end;     

procedure TcxCustomTextFlowPanel.Loaded;
begin
  inherited;
  DoContentUpdate;
end;

function TcxCustomTextFlowPanel.MouseOverContent: Boolean;
var
  APos: TPoint;
begin
  Result := HasImage or ((Caption <> '') or (Title <> ''));
  if Result then
  begin
    GetCursorPos(APos);
    APos := ScreenToClient(APos);
    Result := (APos.X > ContentOffset) and (APos.X < Width - ContentOffset)
      and (APos.Y > ContentOffset) and (APos.Y < Height - ContentOffset);
  end;
end;

procedure TcxCustomTextFlowPanel.Paint;
begin
  inherited;
  DoContentUpdate;
end;

procedure TcxCustomTextFlowPanel.PaintCaption;
var
  Offset: Integer;
  ARect: TRect;
  Flags: Cardinal;
begin
  if not HandleAllocated then
    Exit;
  Offset := ContentOffset;
  if HasImage then
    Offset := Offset + Images.Width + ImageOffset;

  Canvas.Font.Assign(Font);
  Canvas.Brush.Style := bsClear;
  Canvas.Font.Color := LookAndFeel.Painter.DefaultContentTextColor;
  
  Canvas.Font.Style := [fsBold];

  ARect := ClientRect;
  ARect.Top := ARect.Top + ContentOffset;
  ARect.Left := ARect.Left + Offset;
  ARect.Right := ARect.Right - ContentOffset;
  ARect.Bottom := ARect.Bottom - ContentOffset;    

  if Enabled then
    Canvas.Font.Color := LookAndFeel.Painter.DefaultContentTextColor
  else
    Canvas.Font.Color := LookAndFeel.Painter.DefaultEditorTextColorEx(esckDisabled);
                                                            
  if Title <> '' then
    DrawText(Canvas.Handle, PChar(Title), Length(Title), ARect,
      DT_NOPREFIX or DT_WORD_ELLIPSIS);
                                    
  Canvas.Font.Style := [];
  if Title <> '' then
    ARect.Top := ARect.Top + 16;

  Flags := DT_WORDBREAK or DT_NOPREFIX or DT_EDITCONTROL;
  if Align in [alClient, alCustom] then
    Flags := Flags or DT_END_ELLIPSIS;

  DrawText(Canvas.Handle, PChar(Caption), Length(Caption), ARect,
    Flags);
end;

procedure TcxCustomTextFlowPanel.PaintImage;
begin
  if HasImage then
    Images.Draw(Self.Canvas, ContentOffset, ContentOffset, ImageIndex,
      Enabled);
end;

procedure TcxCustomTextFlowPanel.ResizeControl;
var
  HOffset, VOffset, AHeight, AWidth, X: Integer;
  ARect, ACalcRect, ACalcRect2: TRect;
  Flags: Cardinal;
begin
  if not HandleAllocated then
    Exit;
  HOffset := ContentOffset;
  VOffset := ContentOffset * 2;
  if Title <> '' then
    VOffset := VOffset + 16;
  if HasImage then
    HOffset := HOffset + Images.Width + ImageOffset;   

  Canvas.Font.Assign(Font);
  Canvas.Brush.Style := bsClear;
  Canvas.Font.Color := clWindowText;

  ARect := ClientRect;
  ARect.Top := ARect.Top + ContentOffset;
  if Title <> '' then
    ARect.Top := ARect.Top + 16;
  ARect.Left := ARect.Left + HOffset;
  ARect.Right := ARect.Right - ContentOffset;
  ARect.Bottom := ARect.Bottom - ContentOffset;

  ACalcRect := ARect;

  Flags := DT_WORDBREAK or DT_NOPREFIX or DT_EDITCONTROL;
  
  DrawText(Canvas.Handle, PChar(Caption), Length(Caption), ACalcRect, Flags or DT_CALCRECT);

  AHeight := VOffset + ACalcRect.Bottom - ACalcRect.Top;

  if (Align in [alNone, alTop, alBottom]) and (Height <> AHeight) then
    Height := AHeight
  else if (Align in [alLeft, alRight]) and (AHeight <> Height) then
  begin
    if AHeight > Height then
    begin
      while AHeight > Height do
      begin
        ACalcRect.Right := ACalcRect.Right + 1;
        ACalcRect2 := ACalcRect;
        DrawText(Canvas.Handle, PChar(Caption), Length(Caption), ACalcRect2, Flags or DT_CALCRECT);
        //can't break a word and will end in an infinite loop, break
        if ACalcRect2.Right > ACalcRect.Right then
          Break;
        AHeight := VOffset + ACalcRect2.Bottom - ACalcRect2.Top;
      end;
    end else
    begin
      while AHeight < Height do
      begin                
        X := ACalcRect.Right;
        ACalcRect.Right := ACalcRect.Right - 1;
        ACalcRect2 := ACalcRect;
        DrawText(Canvas.Handle, PChar(Caption), Length(Caption), ACalcRect2, Flags or DT_CALCRECT);
        //can't break a word and will end in an infinite loop, break
        if ACalcRect2.Right > ACalcRect.Right then
          Break;
        AHeight := VOffset + ACalcRect2.Bottom - ACalcRect2.Top;
        if AHeight > Height then
          ACalcRect.Right := X;
      end;
    end;

    AWidth := ACalcRect.Right - ACalcRect.Left + HOffset;
    if Width <> AWidth then
      Width := AWidth;
  end;
end;

procedure TcxCustomTextFlowPanel.SetCaption(const Value: TCaption);
begin
  if Value <> FCaption then
  begin
    FCaption := Value;
    Invalidate;
  end;
end;

procedure TcxCustomTextFlowPanel.SetConstraints;
begin
  if HasImage then
  begin
    if (ContentOffset * 2) + Images.Width > Constraints.MinWidth then
      Constraints.MinWidth := (ContentOffset * 2) + Images.Width;
    if (ContentOffset * 2) + Images.Height > Constraints.MinHeight then
      Constraints.MinHeight := (ContentOffset * 2) + Images.Height;
  end else
  begin
    Constraints.MinWidth := 24;
    Constraints.MinHeight := 24;
  end;
end;      

procedure TcxCustomTextFlowPanel.SetContentOffset(const Value: Integer);
begin
  if Value <> FContentOffset then
  begin
    FContentOffset := Value;
    Invalidate;
  end;
end;

procedure TcxCustomTextFlowPanel.SetImageIndex(const Value: TImageIndex);
begin
  if Value <> FImageIndex then
  begin
    FImageIndex := Value;
    SetConstraints;
    Invalidate;
  end;
end;   

procedure TcxCustomTextFlowPanel.SetImageOffset(const Value: Integer);
begin 
  if Value <> FImageOffset then
  begin
    FImageOffset := Value;
    Invalidate;
  end;
end;

procedure TcxCustomTextFlowPanel.SetTitle(const Value: TCaption);
begin      
  if Value <> FTitle then
  begin
    FTitle := Value;
    Invalidate;
  end;
end;

procedure TcxCustomTextFlowPanel.WMSetCursor(var Message: TWMSetCursor);
begin
  if (Assigned(Self.OnClick) or (Assigned(Self.Action) and Assigned(Self.Action.OnExecute))) and MouseOverContent then
    Windows.SetCursor(Screen.Cursors[crHandPoint])
  else
    inherited;
end;

end.
