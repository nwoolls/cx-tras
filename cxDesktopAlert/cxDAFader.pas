// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxDAFader;

interface

uses Classes, SyncObjs, Windows, Messages;

const
  WM_SETALPHA = WM_USER + 10;

type

  TcxDAFader = class(TThread)
  private
    FCancelFade: Boolean;
    FCriticalSection: TCriticalSection;
    FFadingOut: Boolean;
    FHandle: HWND;
    FInitialAlpha: Integer;
    FTransparency: Integer;
    function GetCancelFade: Boolean;
    function GetTransparency: Integer;
    procedure SetCancelFade(const Value: Boolean);
    procedure SetTransparency(const Value: Integer);
  protected
    procedure Execute; override;
  public
    constructor Create(const AHandle: HWND); reintroduce;
    destructor Destroy; override;
    procedure FadeIn(AInitialAlpha: Integer);
    procedure FadeOut(AInitialAlpha: Integer);
    property CancelFade: Boolean read GetCancelFade write SetCancelFade default True;
    property Transparency: Integer read GetTransparency write SetTransparency default 20;
  end;

implementation

uses SysUtils;

{ TcxFader }

constructor TcxDAFader.Create(const AHandle: HWND);
begin
  inherited Create(True);
  FCriticalSection := TCriticalSection.Create;
  FCancelFade := True;
  FHandle := AHandle;
  FreeOnTerminate := True;
end;

destructor TcxDAFader.Destroy;
begin
  FreeAndNil(FCriticalSection);
  inherited;
end;

procedure TcxDAFader.Execute;
var
  I, J: Integer;
  HighAlpha: Integer;
begin
  while not Terminated do
  begin
    CancelFade := False;

    J := FInitialAlpha;                           
    if not FFadingOut then
    begin
      HighAlpha := ((100 - FTransparency) * 255) div 100;
      for I := J to HighAlpha do
      begin
        if CancelFade or Terminated then
          Break;
        PostMessage(FHandle, WM_SETALPHA,  I, 0);
        Sleep(10);
      end;
    end else
    begin
      for I := J downto 0 do
      begin
        if CancelFade or Terminated then
          Break;
        PostMessage(FHandle, WM_SETALPHA, I, 0);
        Sleep(10);
      end;
      if not CancelFade then
        PostMessage(FHandle, WM_CLOSE, 0, 0);
    end;

    if not Terminated then
      Suspend;
  end;
end;

procedure TcxDAFader.FadeIn(AInitialAlpha: Integer);
begin
  FInitialAlpha := AInitialAlpha;
  FFadingOut := False;
  Resume;
end;

procedure TcxDAFader.FadeOut(AInitialAlpha: Integer);
begin        
  FInitialAlpha := AInitialAlpha;
  FFadingOut := True;
  Resume;
end;   

function TcxDAFader.GetCancelFade: Boolean;
begin
  FCriticalSection.Enter;
  try
    Result := FCancelFade;
  finally
    FCriticalSection.Leave;
  end;
end;

function TcxDAFader.GetTransparency: Integer;
begin
  FCriticalSection.Enter;
  try
    Result := FTransparency;
  finally
    FCriticalSection.Leave;
  end;
end;

procedure TcxDAFader.SetCancelFade(const Value: Boolean);
begin
  FCriticalSection.Enter;
  try
    FCancelFade := Value;
  finally
    FCriticalSection.Leave;
  end;
end;   

procedure TcxDAFader.SetTransparency(const Value: Integer);
begin
  FCriticalSection.Enter;
  try
    FTransparency := Value;           
  finally
    FCriticalSection.Leave;
  end;
end;

end.
