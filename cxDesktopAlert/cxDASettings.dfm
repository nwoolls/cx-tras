object cxDASettingsForm: TcxDASettingsForm
  Left = 0
  Top = 0
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Desktop Alert Settings'
  ClientHeight = 241
  ClientWidth = 295
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PreviewButton: TcxButton
    Left = 6
    Top = 210
    Width = 63
    Height = 23
    Caption = '&Preview'
    TabOrder = 2
    OnClick = PreviewButtonClick
  end
  object OkButton: TcxButton
    Left = 154
    Top = 210
    Width = 63
    Height = 23
    Caption = 'OK'
    Default = True
    TabOrder = 3
    OnClick = OkButtonClick
  end
  object CancelButton: TcxButton
    Left = 226
    Top = 210
    Width = 63
    Height = 23
    Caption = 'Cancel'
    TabOrder = 4
    OnClick = CancelButtonClick
  end
  object DurationGroupBox: TcxGroupBox
    Left = 6
    Top = 8
    Caption = '&Duration'
    TabOrder = 0
    Height = 98
    Width = 283
    object cxLabel1: TcxLabel
      Left = 9
      Top = 18
      Caption = 'How long should the Desktop Alert appear?'
    end
    object cxLabel3: TcxLabel
      Left = 29
      Top = 44
      Caption = 'Short'
    end
    object cxLabel2: TcxLabel
      Left = 226
      Top = 44
      Caption = 'Long'
    end
    object DurationTrackBar: TcxTrackBar
      Left = 61
      Top = 44
      Position = 7
      Properties.AutoSize = False
      Properties.Max = 30
      Properties.Min = 3
      Properties.ShowTicks = False
      Properties.ThumbHeight = 22
      Properties.ThumbWidth = 12
      Properties.OnChange = DurationTrackBarPropertiesChange
      TabOrder = 3
      Transparent = True
      Height = 31
      Width = 161
    end
    object DurationLabel: TcxLabel
      Left = 115
      Top = 74
      Caption = '7 seconds'
    end
  end
  object TransparencyGroupBox: TcxGroupBox
    Left = 6
    Top = 109
    Caption = '&Transparency'
    TabOrder = 1
    Height = 98
    Width = 283
    object cxLabel5: TcxLabel
      Left = 9
      Top = 18
      Caption = 'How transparent should the Desktop Alert be?'
    end
    object cxLabel6: TcxLabel
      Left = 29
      Top = 44
      Caption = 'Solid'
    end
    object cxLabel7: TcxLabel
      Left = 226
      Top = 44
      Caption = 'Clear'
    end
    object TransparencyTrackBar: TcxTrackBar
      Left = 61
      Top = 44
      Position = 20
      Properties.AutoSize = False
      Properties.Max = 80
      Properties.ShowTicks = False
      Properties.ThumbHeight = 22
      Properties.ThumbWidth = 12
      Properties.OnChange = TransparencyTrackBarPropertiesChange
      TabOrder = 3
      Transparent = True
      Height = 31
      Width = 161
    end
    object TransparencyLabel: TcxLabel
      Left = 98
      Top = 74
      Caption = '20% transparent'
    end
  end
end
