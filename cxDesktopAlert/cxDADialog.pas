// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxDADialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxOG, cxLookAndFeels, ImgList, cxControls, cxCB,
  StdCtrls, Menus, cxContainer, cxLabel, SyncObjs, cxDALabel, cxDAFader,
  cxGraphics, cxLookAndFeelPainters;

type

  TcxDAPosition = (apTopLeft, apTopRight, apBottomLeft, apBottomRight,
    apCenter, apCustom);       
  
  TcxPaintStyle = (psStandard, psOffice10, psOffice11, psUseLookAndFeel);

  TcxAlertForm = class(TForm)
    ButtonPanel: TcxOfficeGizmo;
    CloseButton: TcxOfficeGizmo;
    DAImageList: TImageList;
    DATimeoutTimer: TTimer;
    HolderPanel: TcxOfficeGizmo;
    ImagePanel: TcxOfficeGizmo;
    OptionsButton: TcxOfficeGizmo;
    SplitterPanel: TcxOfficeGizmo;
    XPCloseButton: TcxCloseButton;
    procedure CloseButtonClick(Sender: TObject);
    procedure DATimeoutTimerTimer(Sender: TObject); 
    procedure HandleMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure HandleMouseEnter(Sender: TObject);
    procedure HandleMouseLeave(Sender: TObject);
    procedure HandleMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure HandleMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }     
    FDuration: Integer;
    FFader: TcxDAFader;
    FInitialX, FInitialY: Integer;
    FMessage: string;
    FMouseOver: Boolean;
    FMoving: Boolean;
    FPaintStyle: TcxPaintStyle;
    FPosition: TcxDAPosition;
    FTitle: string;
    FTitleLabel, FCaptionLabel: TcxDALabel;
    FTransparency: Integer;
    procedure LayoutControls;
    procedure CreateLabels;
    function GetLinkColor: TColor;
    procedure HandleCaptionClick(Sender: TObject);
    procedure HandleLabelEnter(Sender: TObject);
    procedure HandleLabelLeave(Sender: TObject);
    procedure SetDuration(const Value: Integer);
    procedure SetMessage(const Value: string);
    procedure SetPaintStyle(const Value: TcxPaintStyle);
    procedure SetPosition(const Value: TcxDAPosition);
    procedure SetTimerDuration;
    procedure SetTitle(const Value: string);
    procedure SetTransparency(const Value: Integer);
    procedure WMSetAlpha(var Message: TMessage); message WM_SETALPHA;
    procedure FadeOut;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    property Fader: TcxDAFader read FFader;
  public
    procedure FadeIn;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property CaptionLabel: TcxDALabel read FCaptionLabel;
    property Duration: Integer read FDuration write SetDuration;
    property Message: string read FMessage write SetMessage;
    property PaintStyle: TcxPaintStyle read FPaintStyle write SetPaintStyle default psUseLookAndFeel;
    property Position: TcxDAPosition read fPosition write SetPosition;
    property Title: string read FTitle write SetTitle;
    property TitleLabel: TcxDALabel read FTitleLabel;
    property Transparency: Integer read FTransparency write SetTransparency;
  end;

var
  cxAlertForm: TcxAlertForm;

implementation

uses cxDASettings, dxThemeManager, Math, dxUxTheme, Types;

{$R *.dfm}     

{ TcxAlertForm }

constructor TcxAlertForm.Create(AOwner: TComponent);
begin
  inherited;
  FPaintStyle := psUseLookAndFeel;
  FFader := TcxDAFader.Create(Self.Handle);

  if Assigned(SetLayeredWindowAttributes) then
  begin
    AlphaBlend := True;
    AlphaBlendValue := 255;
  end;
  SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle, GWL_EXSTYLE)
    or WS_EX_TOOLWINDOW);

  CreateLabels;               
end;

destructor TcxAlertForm.Destroy;
var
  WasSuspended: Boolean;
begin
  WasSuspended := Fader.Suspended;
  Fader.Terminate;
  if WasSuspended then
    Fader.Resume;
  Sleep(20);
  inherited;
end;

procedure TcxAlertForm.CloseButtonClick(Sender: TObject);
begin
  Fader.CancelFade := False;
  Close;
end;

procedure TcxAlertForm.CreateLabels;
begin
  FTitleLabel := TcxDALabel.Create(Self);
  TitleLabel.ShowAccelChar := False;
  TitleLabel.Transparent := True;
  TitleLabel.OnMouseEnter := HandleLabelEnter;
  TitleLabel.OnMouseLeave := HandleLabelLeave;
  TitleLabel.OnMouseDown := HandleMouseDown;
  TitleLabel.OnMouseMove := HandleMouseMove;
  TitleLabel.OnMouseUp := HandleMouseUp;
  TitleLabel.OnClick := HandleCaptionClick;

  TitleLabel.Left := 44;
  TitleLabel.Top := 10;
  TitleLabel.Constraints.MaxWidth := 250;
  TitleLabel.Font.Style := [fsBold];
  TitleLabel.EllipsisPosition := epWordEllipsis;
  TitleLabel.Parent := HolderPanel;

  FCaptionLabel := TcxDALabel.Create(Self);
  CaptionLabel.ShowAccelChar := False;
  CaptionLabel.Transparent := True;
  CaptionLabel.OnMouseEnter := HandleLabelEnter;
  CaptionLabel.OnMouseLeave := HandleLabelLeave;
  CaptionLabel.OnMouseDown := HandleMouseDown;
  CaptionLabel.OnMouseMove := HandleMouseMove;
  CaptionLabel.OnMouseUp := HandleMouseUp;
  CaptionLabel.OnClick := HandleCaptionClick;
  
  CaptionLabel.Left := 44;
  CaptionLabel.Top := 24;
  CaptionLabel.Constraints.MaxWidth := 277;
  CaptionLabel.Font.Style := [];
  CaptionLabel.WordWrap := True;
  CaptionLabel.EllipsisPosition := epEndEllipsis;
  CaptionLabel.Parent := HolderPanel;
end;

procedure TcxAlertForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.WndParent := GetDesktopWindow;
end;

type
  TcxOfficeGizmoAccess = class(TcxOfficeGizmo);
                                           
procedure TcxAlertForm.LayoutControls;
begin                              
  //force a paint to get the splitter to resize if it's skinned
  TcxOfficeGizmoAccess(SplitterPanel).Paint;
  
  XPCloseButton.Top := SplitterPanel.Height + 1;
  CloseButton.Top := XPCloseButton.Top;
end;

procedure TcxAlertForm.FadeIn;
begin
  LayoutControls;

  SetTimerDuration;
  if Assigned(SetLayeredWindowAttributes) then
  begin
    Fader.FadeIn(AlphaBlendValue);
  end else
  begin
    Show;
    DATimeoutTimer.Interval := DATimeoutTimer.Interval * 2;
    DATimeoutTimer.Enabled := True;
  end;
end;

procedure TcxAlertForm.FadeOut;
begin   
  if Assigned(SetLayeredWindowAttributes) then
  begin
    Fader.FadeOut(AlphaBlendValue);
  end else  
  begin
    Close;
  end;
end;

procedure TcxAlertForm.DATimeoutTimerTimer(Sender: TObject);
begin
  DATimeoutTimer.Enabled := False;
  if not FMouseOver then
    FadeOut;
end;

function TcxAlertForm.GetLinkColor: TColor;
begin
  if (OpenTheme(totRebar) <> 0) and
    ((HolderPanel.PaintStyle = cxOG.psOffice11) or
    ((HolderPanel.PaintStyle = cxOG.psUseLookAndFeel) and
    (HolderPanel.LookAndFeel.Kind = lfOffice11) and
    not HolderPanel.LookAndFeel.NativeStyle)) then
  begin
    Result := $00B23D00;
  end else
    Result := ColorToRGB(clBlue);
end;

procedure TcxAlertForm.HandleCaptionClick(Sender: TObject);
begin
  if Assigned(OnClick) then
  begin              
    CloseButtonClick(Sender);
    OnClick(Sender);
  end;
end;

procedure TcxAlertForm.HandleLabelEnter(Sender: TObject);
begin
  if Assigned(OnClick) then
  begin
    TitleLabel.Cursor := crHandPoint;
    TitleLabel.Font.Style := TitleLabel.Font.Style + [fsUnderline];
    TitleLabel.Font.Color := GetLinkColor;

    CaptionLabel.Cursor := crHandPoint;
    CaptionLabel.Font.Style := CaptionLabel.Font.Style + [fsUnderline];
    CaptionLabel.Font.Color := GetLinkColor;
  end;
end;         

procedure TcxAlertForm.HandleLabelLeave(Sender: TObject);
begin
  if Assigned(OnClick) then
  begin
    TitleLabel.Font.Style := TitleLabel.Font.Style - [fsUnderline];
    TitleLabel.Font.Color := clWindowText;
    
    CaptionLabel.Font.Style := CaptionLabel.Font.Style - [fsUnderline];
    CaptionLabel.Font.Color := clWindowText;
  end;
end;

procedure TcxAlertForm.HandleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FMoving := True;
  FInitialX := X;
  FInitialY := Y;
end; 

procedure TcxAlertForm.HandleMouseEnter(Sender: TObject);
begin
  FMouseOver := True;

  if Assigned(Fader) then
    if not Fader.Suspended then
    begin
      Fader.CancelFade := True;
      Sleep(10);
    end;

  if AlphaBlendValue <> 255 then
  begin
    AlphaBlendValue := 255;
    Update;
  end;
end;

procedure TcxAlertForm.HandleMouseLeave(Sender: TObject);
var
  APoint: TPoint;
begin
  FMouseOver := False;
  if not FMoving then
  begin
    GetCursorPos(APoint);
    if not PtInRect(Rect(Left, Top, Left + Width, Top + Height), APoint) then
    begin
      AlphaBlendValue := ((100 - Transparency) * 255) div 100;
      Update;

      if not DATimeoutTimer.Enabled then
        FadeOut;
    end;
  end;
end;   

procedure TcxAlertForm.HandleMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  ALeft, ATop: Integer;
  ARect: TRect;
begin
  if FMoving then
  begin
    ALeft := Left + (X - FInitialX);  
    ATop := Top + (Y - FInitialY);

    ARect := GetDesktopWorkArea(GetMouseCursorPos);
                          
    if ALeft < ARect.Left then
      ALeft := ARect.Left;
    if ATop < ARect.Top then
      ATop := ARect.Top;
    if ALeft + Width > ARect.Right then
      ALeft := ARect.Right - Width;
    if ATop + Height > ARect.Bottom then
      ATop := ARect.Bottom - Height;

    Left := ALeft;
    Top := ATop;

    Update;
  end;
end;   

procedure TcxAlertForm.HandleMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FMoving := False;
  if not FMouseOver then
    HandleMouseLeave(Sender);
end;

procedure TcxAlertForm.SetDuration(const Value: Integer);
begin
  FDuration := Value;       
  SetTimerDuration;
end;   

procedure TcxAlertForm.SetMessage(const Value: string);
begin
  FMessage := Value;
  CaptionLabel.Caption := Value;
  CaptionLabel.AutoSize := False;
  CaptionLabel.Width := 277;
  CaptionLabel.AutoSize := True;
end;

procedure TcxAlertForm.SetPaintStyle(const Value: TcxPaintStyle);
begin
  FPaintStyle := Value;
  case PaintStyle of
    psStandard:
    begin
      HolderPanel.PaintStyle := cxOG.psStandard;
      XPCloseButton.PaintStyle := cxCB.psStandard;
    end;
    psOffice10:
    begin
      HolderPanel.PaintStyle := cxOG.psOffice10;
      XPCloseButton.PaintStyle := cxCB.psOffice10;
    end;
    psOffice11:
    begin
      HolderPanel.PaintStyle := cxOG.psOffice11;
      XPCloseButton.PaintStyle := cxCB.psOffice11;
    end;
  else
    HolderPanel.PaintStyle := cxOG.psUseLookAndFeel;
    XPCloseButton.PaintStyle := cxCB.psUseLookAndFeel;
  end;

  XPCloseButton.Visible := (AreVisualStylesAvailable and
    ((PaintStyle = psStandard) and IsAppThemed) or
    ((PaintStyle = psUseLookAndFeel) and
    (HolderPanel.LookAndFeel.NativeStyle)))
    or (HolderPanel.LookAndFeel.SkinName <> '');

  CloseButton.Visible := not XPCloseButton.Visible;
end;

procedure TcxAlertForm.SetPosition(const Value: TcxDAPosition);
begin
  fPosition := Value;
  case Position of
    apTopLeft:
    begin
      Top := 0;
      Left := 0;
    end;
    apTopRight:
    begin
      Top := 0;
      Left := Screen.WorkAreaLeft + Screen.WorkAreaWidth - Width;
    end;
    apBottomLeft:
    begin
      Top := Screen.WorkAreaTop + Screen.WorkAreaHeight - Height;
      Left := 0;
    end;
    apBottomRight:
    begin
      Top := Screen.WorkAreaTop + Screen.WorkAreaHeight - Height;
      Left := Screen.WorkAreaLeft + Screen.WorkAreaWidth - Width;
    end;
    apCenter:
    begin
      Top := ((Screen.WorkAreaTop + Screen.WorkAreaHeight) div 2) -
        (Height div 2);
      Left := ((Screen.WorkAreaLeft + Screen.WorkAreaWidth) div 2) -
        (Width div 2);
    end;
  end;
end;

procedure TcxAlertForm.SetTimerDuration;
begin
  DATimeoutTimer.Interval := (Duration * 1000) -
    (20 * ((100 - Transparency) * 255) div 100);
end;

procedure TcxAlertForm.SetTitle(const Value: string);
begin
  FTitle := Value;
  TitleLabel.Caption := Value;   
  TitleLabel.AutoSize := False;
  TitleLabel.Width := 250;
  TitleLabel.AutoSize := True;
end;

procedure TcxAlertForm.SetTransparency(const Value: Integer);
begin
  FTransparency := Value;    
  SetTimerDuration;
  Fader.Transparency := Value;
end;

procedure TcxAlertForm.WMSetAlpha(var Message: TMessage);
begin
  AlphaBlendValue := Message.WParam;
  if AlphaBlendValue >= ((100 - Transparency) * 255) div 100 then
    DATimeoutTimer.Enabled := True;
end;

end.
