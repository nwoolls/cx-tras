// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxDASettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, Menus, cxLookAndFeelPainters,
  cxLookAndFeels, cxButtons, cxControls, cxContainer, cxEdit, cxGroupBox,
  cxLabel, cxTrackBar, cxDA, cxGraphics;

type
  TcxDASettingsForm = class(TForm)
    CancelButton: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    DurationGroupBox: TcxGroupBox;
    DurationLabel: TcxLabel;
    DurationTrackBar: TcxTrackBar;
    OkButton: TcxButton;
    PreviewButton: TcxButton;
    TransparencyGroupBox: TcxGroupBox;
    TransparencyLabel: TcxLabel;
    TransparencyTrackBar: TcxTrackBar;
    procedure CancelButtonClick(Sender: TObject);
    procedure DurationTrackBarPropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OkButtonClick(Sender: TObject);
    procedure PreviewButtonClick(Sender: TObject);
    procedure TransparencyTrackBarPropertiesChange(Sender: TObject);
  private
    FDesktopAlert: TcxCustomDesktopAlert;
    procedure SetDesktopAlert(const Value: TcxCustomDesktopAlert);
  public
    { Public declarations }
    property DesktopAlert: TcxCustomDesktopAlert read FDesktopAlert write SetDesktopAlert;
  end;

var
  cxDASettingsForm: TcxDASettingsForm;

implementation

{$R *.dfm}       

{ TcxDASettingsForm }

procedure TcxDASettingsForm.CancelButtonClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TcxDASettingsForm.DurationTrackBarPropertiesChange(Sender: TObject);
begin
  DurationLabel.Caption := IntToStr(DurationTrackBar.Position) + ' seconds';
end;

procedure TcxDASettingsForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DesktopAlert.Stop;
end;

procedure TcxDASettingsForm.OkButtonClick(Sender: TObject);
begin
  DesktopAlert.Duration := DurationTrackBar.Position;
  DesktopAlert.Transparency := TransparencyTrackBar.Position;
  ModalResult := mrOk;
end;

procedure TcxDASettingsForm.PreviewButtonClick(Sender: TObject);
begin
  DesktopAlert.Preview(DurationTrackBar.Position,
    TransparencyTrackBar.Position);
end;

procedure TcxDASettingsForm.SetDesktopAlert(const Value: TcxCustomDesktopAlert);
begin
  FDesktopAlert := Value;
  DurationTrackBar.Position := DesktopAlert.Duration;
  TransparencyTrackBar.Position := DesktopAlert.Transparency;
end;

procedure TcxDASettingsForm.TransparencyTrackBarPropertiesChange(
  Sender: TObject);
begin
  TransparencyLabel.Caption := IntToStr(TransparencyTrackBar.Position) +
    '% transparent';
end;

end.
