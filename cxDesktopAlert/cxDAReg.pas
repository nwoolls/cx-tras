// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxDAReg;  

{$R cxDesktopAlert_Glyphs.res}

interface           

procedure Register;

implementation

uses cxPropEditors, cxDA, Controls, Classes, ImgList, Math, SysUtils,
  DesignEditors, DesignIntf;

type

  TDAAlertImageIndexProperty = class(TImageIndexProperty)
  public
    function GetImages: TCustomImageList; override;
  end;

  TDAButtonImageIndexProperty = class(TImageIndexProperty)
  public
    function GetImages: TCustomImageList; override;
  end;       

  TcxDAButtonsItemProperty = class(TNestedProperty)
  private
    FElement: Integer;
  protected
    constructor Create(Parent: TPropertyEditor; AElement: Integer); reintroduce;
    property Element: Integer read FElement;
  public
    function GetAttributes: TPropertyAttributes; override;
    function GetName: string; override;
    function GetValue: string; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

  TcxDAButtonsProperty = class(TNestedProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetProperties(Proc: TGetPropProc); override;
    function GetValue: string; override;
  end;

  TcxDesktopAlertEditor = class(TComponentEditor)
  private
    procedure DoSettings;
  public
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;
    procedure ExecuteVerb(Index: Integer); override;
  end;

procedure Register;
begin
  RegisterComponents('cx-tras', [TcxDesktopAlert]);
  RegisterPropertyEditor(TypeInfo(TImageIndex), TcxDAAlert, 'ImageIndex',
    TDAAlertImageIndexProperty);
  RegisterPropertyEditor(TypeInfo(TImageIndex), TcxDAButton, 'ImageIndex',
    TDAButtonImageIndexProperty);
  RegisterPropertyEditor(TypeInfo(TcxDAButtonSet), TcxDAAlert, 'Buttons',
    TcxDAButtonsProperty);
  RegisterComponentEditor(TcxDesktopAlert, TcxDesktopAlertEditor);
end;

{ TDAAlertImageIndexProperty }

function TDAAlertImageIndexProperty.GetImages: TCustomImageList;
begin
  Result := nil;
  if GetComponent(0) is TcxDAAlert then
    Result := TcxDesktopAlert(TcxDAAlerts(TcxDAAlert(
      GetComponent(0)).Collection).Owner).Images;
end;

{ TDAButtonImageIndexProperty }

function TDAButtonImageIndexProperty.GetImages: TCustomImageList;
begin
  Result := nil;
  if GetComponent(0) is TcxDAButton then
    Result := TcxDesktopAlert(TcxDAButtons(TcxDAButton(
      GetComponent(0)).Collection).Owner).ButtonImages; 
end;

{ TcxDAButtonsProperty }

function TcxDAButtonsProperty.GetAttributes: TPropertyAttributes;
begin
  with TcxDesktopAlert(TcxDAAlerts(TcxDAAlert(
    GetComponent(0)).Collection).Owner) do
    if Buttons.Count > 0 then
      Result := [paMultiSelect, paSubProperties, paReadOnly, paRevertable]
    else
      Result := [paMultiSelect, paReadOnly, paRevertable];
end;

procedure TcxDAButtonsProperty.GetProperties(Proc: TGetPropProc);
var
  I: Integer;
begin
  with TcxDesktopAlert(TcxDAAlerts(TcxDAAlert(
    GetComponent(0)).Collection).Owner) do
    for I := 0 to Buttons.Count - 1 do
      Proc(TcxDAButtonsItemProperty.Create(Self, I));
end;

function TcxDAButtonsProperty.GetValue: string;
var
  S: Integer;
  I: Integer;
begin
  S := GetOrdValue;
  Result := '[';
  with TcxDesktopAlert(TcxDAAlerts(TcxDAAlert(
    GetComponent(0)).Collection).Owner) do
  begin
    for I := 0 to Buttons.Count - 1 do
      if (Trunc(Power(2, I)) and S) > 0 then
      begin
        if Length(Result) <> 1 then
          Result := Result + ',';

        Result := Result + Buttons[I].Name; 
      end;
  end;

  Result := Result + ']';
end;

{ TcxDAButtonsItemProperty }

constructor TcxDAButtonsItemProperty.Create(Parent: TPropertyEditor;
  AElement: Integer);
begin
  inherited Create(Parent);
  FElement := AElement;
end;       

var
  BooleanIdents: array [Boolean] of string = ('False', 'True');

procedure TcxDAButtonsItemProperty.GetValues(Proc: TGetStrProc);
begin
  Proc(BooleanIdents[False]);
  Proc(BooleanIdents[True]);
end;

function TcxDAButtonsItemProperty.GetValue: string;
begin
  Result := BooleanIdents[(Trunc(Power(2, FElement)) and GetOrdValue) > 0];
end;

function TcxDAButtonsItemProperty.GetName: string;
begin
  with TcxDesktopAlert(TcxDAAlerts(TcxDAAlert(
    GetComponent(0)).Collection).Owner) do
    Result := Buttons[FElement].Name;
end;

procedure TcxDAButtonsItemProperty.SetValue(const Value: string);
var
  I: Integer;
begin
  I := GetOrdValue;
  if CompareText(Value, BooleanIdents[True]) = 0 then
    SetOrdValue(I or Trunc(Power(2, FElement)))
  else
    SetOrdValue(I and (not (Trunc(Power(2, FElement))) and I));
end;

function TcxDAButtonsItemProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList];
end;

{ TcxDesktopAlertEditor }

procedure TcxDesktopAlertEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0: DoSettings;
  end;
end;

function TcxDesktopAlertEditor.GetVerbCount: Integer;
begin
  Result := 1;
end;

procedure TcxDesktopAlertEditor.DoSettings;
begin      
  TcxDesktopAlert(Component).ShowSettings;
  Designer.Modified;
end;

function TcxDesktopAlertEditor.GetVerb(Index: Integer): string;
begin            
  case Index of
    0: Result := 'Desktop Alert Settings...';
  end;
end;

end.
