// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxDA;

interface

uses
  cxOG, cxDADialog, ActnList, Contnrs, Classes, ImgList, Windows, Menus,
  Controls, Forms;

type        
  TcxDAButton = class;

  TcxDAButtonNotifier = class(TComponent)
  private
    FButton: TcxDAButton;
    procedure SetButton(const Value: TcxDAButton);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property Button: TcxDAButton read FButton write SetButton;
  end;

  TcxDAButton = class(TCollectionItem)
  private
    FActionLink: TActionLink;
    FHint: string;
    FImageIndex: TImageIndex;
    FName: string;         
    FNotifier: TcxDAButtonNotifier;
    FOnClick: TNotifyEvent;
    procedure ActionChange(Sender: TObject; CheckDefaults: Boolean);
    procedure DoActionChange(Sender: TObject);
    function GetAction: TBasicAction;
    procedure SetAction(const Value: TBasicAction);
    procedure SetHint(const Value: string);
    procedure SetImageIndex(Value: TImageIndex);
  protected
    function GetDisplayName: string; override;
    property ActionLink: TActionLink read FActionLink write FActionLink;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override; 
    property Name: string read FName;
  published
    property Action: TBasicAction read GetAction write SetAction;
    property Hint: string read FHint write SetHint;
    property ImageIndex: TImageIndex read FImageIndex write SetImageIndex default -1;
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
  end;

  TcxDAButtons = class(TOwnedCollection)
  private
    function GetItem(I: Integer): TcxDAButton;
  public
    function Add: TcxDAButton;
    property Items[I: Integer]: TcxDAButton read GetItem; default;
  end;

  TcxDAButtonSet = DWORD;

  TcxDAAlert = class;

  TcxDAAlertNotifier = class(TComponent)
  private
    FAlert: TcxDAAlert;
    procedure SetAlert(const Value: TcxDAAlert);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property Alert: TcxDAAlert read FAlert write SetAlert;
  end;

  TcxDAAlert = class(TCollectionItem)
  private
    FActionLink: TActionLink;
    FButtons: TcxDAButtonSet;
    FImageIndex: TImageIndex;
    FName: string;
    FNotifier: TcxDAAlertNotifier;
    FOnClick: TNotifyEvent;
    FOnOptionsClick: TNotifyEvent;
    FOptionsButton: Boolean;
    FOptionsPopup: TPopupMenu;
    procedure ActionChange(Sender: TObject; CheckDefaults: Boolean);
    procedure DoActionChange(Sender: TObject);
    function GetAction: TBasicAction;
    procedure HandleOptionsClick(Sender: TObject);
    procedure SetAction(const Value: TBasicAction);
    procedure SetButtons(const Value: TcxDAButtonSet);
    procedure SetImageIndex(Value: TImageIndex);
    procedure SetOptionsButton(const Value: Boolean);
    procedure SetOptionsPopup(Value: TPopupMenu);
  protected
    function GetDisplayName: string; override;  
    property ActionLink: TActionLink read FActionLink write FActionLink;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property Name: string read FName;
  published
    property Action: TBasicAction read GetAction write SetAction; 
    property Buttons: TcxDAButtonSet read FButtons write SetButtons;
    property ImageIndex: TImageIndex read FImageIndex write SetImageIndex default -1;
    property OptionsButton: Boolean read FOptionsButton write SetOptionsButton default False;
    property OptionsPopup: TPopupMenu read FOptionsPopup write SetOptionsPopup;
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    property OnOptionsClick: TNotifyEvent read FOnOptionsClick write FOnOptionsClick;
  end;

  TcxDAAlerts = class(TOwnedCollection)
  private
    function GetItem(I: Integer): TcxDAAlert;
  public
    function Add: TcxDAAlert;
    property Items[I: Integer]: TcxDAAlert read GetItem; default;
  end;

  TcxDAQueuedAlert = class
  private
    FAlert: TcxDAAlert;
    FDuration: Integer;
    FMessage: string;
    FTitle: string;
    FTransparency: Integer;
    procedure SetAlert(const Value: TcxDAAlert);
    procedure SetDuration(const Value: Integer);
    procedure SetMessage(const Value: string);
    procedure SetTitle(const Value: string);
    procedure SetTransparency(const Value: Integer);
  public
    constructor Create; virtual;
    property Alert: TcxDAAlert read FAlert write SetAlert;
    property Duration: Integer read FDuration write SetDuration;
    property Message: string read FMessage write SetMessage;
    property Title: string read FTitle write SetTitle;
    property Transparency: Integer read FTransparency write SetTransparency;
  end;

  TcxCustomDesktopAlert = class(TComponent)
  private
    FAlerts: TcxDAAlerts;
    FButtonImages: TCustomImageList;
    FButtons: TcxDAButtons;
    FDialogLeft: Integer;
    FDialogTop: Integer;
    FDuration: Integer;
    FForm: TcxAlertForm;
    FImages: TCustomImageList;
    FPaintStyle: TcxPaintStyle;
    fPosition: TcxDAPosition;
    FQueue: TObjectList;
    FTransparency: Integer;
    function GetForm: TcxAlertForm;
    procedure HandleAfterAlert(Sender: TObject; var Action: TCloseAction);
    procedure HandleWaiterComlete(Sender: TObject);
    procedure ProcessAlertQueue;
    procedure SetAlerts(const Value: TcxDAAlerts);
    procedure SetButtonImages(const Value: TCustomImageList);
    procedure SetButtons(const Value: TcxDAButtons);
    procedure SetDialogLeft(const Value: Integer);
    procedure SetDialogTop(const Value: Integer);
    procedure SetDuration(const Value: Integer);
    procedure SetImages(const Value: TCustomImageList);
    procedure SetPaintStyle(const Value: TcxPaintStyle);
    procedure SetPosition(const Value: TcxDAPosition);
    procedure SetTransparency(const Value: Integer);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property Form: TcxAlertForm read GetForm;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Execute(AAlertIndex: Integer; const ATitle, AMessage: string; ADuration: Integer = -1; ATransparency: Integer = -1): Integer;
    procedure Preview(ADuration, ATransparency: Integer);
    procedure ShowSettings;
    function Stop: Integer;
    property Alerts: TcxDAAlerts read FAlerts write SetAlerts;
    property ButtonImages: TCustomImageList read FButtonImages write SetButtonImages;
    property Buttons: TcxDAButtons read FButtons write SetButtons;
    property DialogLeft: Integer read FDialogLeft write SetDialogLeft default 0;
    property DialogTop: Integer read FDialogTop write SetDialogTop default 0;
    property Duration: Integer read FDuration write SetDuration default 7;
    property Images: TCustomImageList read FImages write SetImages; 
    property PaintStyle: TcxPaintStyle read FPaintStyle write SetPaintStyle default psUseLookAndFeel;
    property Position: TcxDAPosition read fPosition write SetPosition default apBottomRight;
    property Transparency: Integer read FTransparency write SetTransparency default 20;
  end;

  TcxDesktopAlert = class(TcxCustomDesktopAlert)
  published
    property Alerts;
    property ButtonImages;
    property Buttons;
    property DialogLeft;
    property DialogTop;
    property Duration;
    property Images;     
    property PaintStyle;
    property Position;
    property Transparency;
  end;

var
  cxAlertForm: TcxAlertForm;

implementation

uses cxDASettings, Math, cxDAWaiter, SysUtils;

{ TcxCustomDesktopAlert }

constructor TcxCustomDesktopAlert.Create(AOwner: TComponent);
begin
  inherited;
  FForm := nil;
  FPaintStyle := psUseLookAndFeel;
  FButtons := TcxDAButtons.Create(Self, TcxDAButton);
  FAlerts := TcxDAAlerts.Create(Self, TcxDAAlert);
  FQueue := TObjectList.Create;

  FDuration := 7;
  FTransparency := 20;
  FPosition := apBottomRight;
end;

destructor TcxCustomDesktopAlert.Destroy;
begin
  FreeAndNil(FQueue);
  FreeAndNil(FForm);
  FreeAndNil(FAlerts);
  FreeAndNil(FButtons);
  inherited;
end;

function TcxCustomDesktopAlert.Execute(AAlertIndex: Integer; const ATitle,
    AMessage: string; ADuration: Integer = -1;
    ATransparency: Integer = -1): Integer;
var
  QueuedAlert: TcxDAQueuedAlert;
begin
  QueuedAlert := TcxDAQueuedAlert.Create;
  QueuedAlert.Message := AMessage;
  QueuedAlert.Title := ATitle;  
  QueuedAlert.Duration := ADuration;
  QueuedAlert.Transparency := ATransparency;

  if (AAlertIndex >= 0) and (AAlertIndex < Alerts.Count) then
    QueuedAlert.Alert := Alerts[AAlertIndex];

  Result := FQueue.Add(QueuedAlert);

  if Result = 0 then
    ProcessAlertQueue;
end;

function TcxCustomDesktopAlert.GetForm: TcxAlertForm;
begin
  if not Assigned(FForm) then
  begin
    FForm := TcxAlertForm.Create(nil);
    FForm.OnClose := HandleAfterAlert;
  end;
  Result := FForm;
end;

procedure TcxCustomDesktopAlert.HandleAfterAlert(Sender: TObject;
  var Action: TCloseAction);
var
  Waiter: TcxDAWaiter;
begin
  Waiter := TcxDAWaiter.Create(True);
  Waiter.OnTerminate := HandleWaiterComlete;
  Waiter.FreeOnTerminate := True;
  Waiter.Resume;
end;    

procedure TcxCustomDesktopAlert.HandleWaiterComlete(Sender: TObject);
begin
  if FQueue.Count > 0 then
  begin
    FQueue.Delete(0);
    if FQueue.Count > 0 then
      ProcessAlertQueue;
  end;
end;

procedure TcxCustomDesktopAlert.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
  begin
    if AComponent = Images then
      Images := nil;   
    if AComponent = ButtonImages then
      ButtonImages := nil;
  end;
end;

procedure TcxCustomDesktopAlert.Preview(ADuration, ATransparency: Integer);
var
  Index: Integer;
begin
  if Alerts.Count > 0 then
    Index := 0
  else
    Index := -1;
  Execute(Index, 'Desktop Alert Preview',
    'This is a preview of the Desktop Alert.',
    ADuration, ATransparency);
end;

procedure TcxCustomDesktopAlert.ProcessAlertQueue;
var                                  
  QueuedAlert: TcxDAQueuedAlert;
  
  procedure PrepareButtons;
  var
    Button: TcxOfficeGizmo;  
    I, Count: Integer;
  begin
    Count := 0;
    for I := 0 to Buttons.Count - 1 do
    begin
      if (Trunc(Power(2, i)) and QueuedAlert.Alert.Buttons) > 0 then
      begin
        Button := TcxOfficeGizmo.Create(Form.ButtonPanel);
        Button.Images := ButtonImages;
        Button.ImageIndex := Buttons[I].ImageIndex;
        Button.ImageAlignment := iaLeft;
        Button.ImageOffset := 0;
        Button.Transparent := True;
        Button.Kind := gkButton;
        Button.Hint := Buttons[I].Hint;
        Button.Height := 18;
        Button.Width := 18;
        Button.Top := 0;
        Button.Left := Count * 18;
        Button.OnClick := Buttons[I].OnClick;
        Button.Parent := Form.ButtonPanel;
        Inc(Count);
      end;
    end;
    Form.ButtonPanel.Width := 18 * Count;
  end;

  procedure ApplyAlert;
  begin
    Form.ImagePanel.ImageIndex := QueuedAlert.Alert.ImageIndex;
    Form.OptionsButton.Visible := QueuedAlert.Alert.OptionsButton;
    Form.OptionsButton.OnClick := QueuedAlert.Alert.HandleOptionsClick;
    Form.OnClick := QueuedAlert.Alert.OnClick;

    PrepareButtons;
  end;
begin
  Form.ImagePanel.Images := Images;
  Form.AlphaBlendValue := 0;

  Form.PaintStyle := PaintStyle;

  Form.Position := Position;
  if Position = apCustom then
  begin
    Form.Top := DialogTop;
    Form.Left := DialogLeft;
  end;

  while Form.ButtonPanel.ControlCount > 0 do
    Form.ButtonPanel.Controls[0].Free;

  QueuedAlert := TcxDAQueuedAlert(FQueue[0]);

  Form.Title := QueuedAlert.Title;
  Form.Message := QueuedAlert.Message;

  if QueuedAlert.Duration > -1 then
    Form.Duration := QueuedAlert.Duration
  else
    Form.Duration := Duration;

  if QueuedAlert.Transparency > -1 then
    Form.Transparency := QueuedAlert.Transparency
  else
    Form.Transparency := Transparency;

  if Assigned(QueuedAlert.Alert) then
    ApplyAlert;

  Form.Show;
  Form.FadeIn;
end;

procedure TcxCustomDesktopAlert.SetAlerts(const Value: TcxDAAlerts);
begin
  if FAlerts <> Value then
  begin
    FAlerts := Value;
  end;
end;

procedure TcxCustomDesktopAlert.SetButtonImages(const Value: TCustomImageList);
begin
  if FButtonImages <> Value then
  begin
    FButtonImages := Value;
    if Assigned(FButtonImages) then
      FButtonImages.FreeNotification(Self);
  end;
end;

procedure TcxCustomDesktopAlert.SetButtons(const Value: TcxDAButtons);
begin
  if FButtons <> Value then
  begin
    FButtons := Value;
  end;
end;

procedure TcxCustomDesktopAlert.SetDialogLeft(const Value: Integer);
begin       
  if FDialogLeft <> Value then
  begin
    FDialogLeft := Value;
    Position := apCustom;
  end;
end;

procedure TcxCustomDesktopAlert.SetDialogTop(const Value: Integer);
begin
  if FDialogTop <> Value then
  begin
    FDialogTop := Value;   
    Position := apCustom;
  end;
end;

procedure TcxCustomDesktopAlert.SetDuration(const Value: Integer);
begin
  if FDuration <> Value then
  begin
    FDuration := Value;
  end;
end;

procedure TcxCustomDesktopAlert.SetImages(const Value: TCustomImageList);
begin         
  if FImages <> Value then
  begin
    FImages := Value;
    if Assigned(FImages) then
      FImages.FreeNotification(Self);
  end;
end;

procedure TcxCustomDesktopAlert.SetPaintStyle(const Value: TcxPaintStyle);
begin
  if Value <> FPaintStyle then
  begin
    FPaintStyle := Value;
  end;
end;

procedure TcxCustomDesktopAlert.SetPosition(const Value: TcxDAPosition);
begin
  if FPosition <> Value then
  begin
    FPosition := Value;
  end;
end;

procedure TcxCustomDesktopAlert.SetTransparency(const Value: Integer);
begin
  if FTransparency <> Value then
  begin
    FTransparency := Value;
  end;
end;

procedure TcxCustomDesktopAlert.ShowSettings;
begin
  cxDASettingsForm := TcxDASettingsForm.Create(nil);
  try
    cxDASettingsForm.DesktopAlert := Self;
    cxDASettingsForm.ShowModal;
  finally
    FreeAndNil(cxDASettingsForm);
  end;
end;

function TcxCustomDesktopAlert.Stop: Integer;
begin
  Result := FQueue.Count;
  FQueue.Clear;
  if Assigned(FForm) then
    Form.Close;
end;

{ TcxDAButton }

constructor TcxDAButton.Create(Collection: TCollection);
  function GetUniqueName: string;
  var
    I, J: Integer;
    Unique: Boolean;
  begin
    for I := 1 to MaxInt do
    begin
      Result := 'cxDAButton' + IntToStr(I);
      Unique := True;
      for J := 0 to Collection.Count - 1 do
        if TcxDAButton(Collection.Items[J]).Name = Result then
        begin
          Unique := False;
          Break;
        end;
      if Unique then
        Exit;
    end;
  end;    
begin
  inherited Create(Collection);
  FNotifier := TcxDAButtonNotifier.Create(nil);
  FNotifier.Button := Self;
  FName := GetUniqueName;
end;

destructor TcxDAButton.Destroy;
begin
  FreeAndNil(FNotifier);
  inherited;
end;       

procedure TcxDAButton.ActionChange(Sender: TObject; CheckDefaults: Boolean);
begin
  if Sender is TCustomAction then
    with TCustomAction(Sender) do
    begin
      if not CheckDefaults or (Self.ImageIndex = -1) then
        Self.ImageIndex := ImageIndex;
      if not CheckDefaults or (Self.Hint = '') then
        Self.Hint := Hint;
      if not CheckDefaults or not Assigned(Self.OnClick) then
        Self.OnClick := OnExecute;
    end;
end;

procedure TcxDAButton.DoActionChange(Sender: TObject);
begin
  if Sender = Action then ActionChange(Sender, False);
end; 

function TcxDAButton.GetAction: TBasicAction;
begin
  if ActionLink <> nil then
    Result := ActionLink.Action else
    Result := nil;
end;

function TcxDAButton.GetDisplayName: string;
begin
  Result := FName;
end;

procedure TcxDAButton.SetAction(const Value: TBasicAction);
begin
  if Value = nil then
  begin
    ActionLink.Free;
    ActionLink := nil;
  end
  else
  begin
    if ActionLink = nil then
      ActionLink := TActionLink.Create(Self);
    ActionLink.Action := Value;
    ActionLink.OnChange := DoActionChange;
    ActionChange(Value, csLoading in Value.ComponentState);
    Value.FreeNotification(FNotifier);
  end;
end;

procedure TcxDAButton.SetHint(const Value: string);
begin  
  if FHint <> Value then
  begin
    FHint := Value;
  end;
end;

procedure TcxDAButton.SetImageIndex(Value: TImageIndex);
begin
  if FImageIndex <> Value then
  begin
    FImageIndex := Value;
  end;
end;

{ TcxDAButtons }

function TcxDAButtons.Add: TcxDAButton;
begin
  Result := inherited Add as TcxDAButton;
end;

function TcxDAButtons.GetItem(I: Integer): TcxDAButton;
begin
  Result := inherited Items[I] as TcxDAButton;
end;

{ TcxDAAlert }

constructor TcxDAAlert.Create(Collection: TCollection);
  function GetUniqueName: string;
  var
    I, J: Integer;
    Unique: Boolean;
  begin
    for I := 1 to MaxInt do
    begin
      Result := 'cxDAAlert' + IntToStr(I);
      Unique := True;
      for J := 0 to Collection.Count - 1 do
        if TcxDAAlert(Collection.Items[J]).Name = Result then
        begin
          Unique := False;
          Break;
        end;
      if Unique then
        Exit;
    end;
  end;   
begin
  inherited Create(Collection);
  FNotifier := TcxDAAlertNotifier.Create(nil);
  FNotifier.Alert := Self;
  FName := GetUniqueName;
  FOptionsButton := False;
end;

destructor TcxDAAlert.Destroy;
begin
  FreeAndNil(FNotifier);
  inherited;
end;  

procedure TcxDAAlert.ActionChange(Sender: TObject; CheckDefaults: Boolean);
begin
  if Sender is TCustomAction then
    with TCustomAction(Sender) do
    begin
      if not CheckDefaults or (Self.ImageIndex = -1) then
        Self.ImageIndex := ImageIndex;
      if not CheckDefaults or not Assigned(Self.OnClick) then
        Self.OnClick := OnExecute;
    end;
end;

procedure TcxDAAlert.DoActionChange(Sender: TObject);
begin
  if Sender = Action then ActionChange(Sender, False);
end;

function TcxDAAlert.GetAction: TBasicAction;
begin
  if ActionLink <> nil then
    Result := ActionLink.Action else
    Result := nil;
end;

function TcxDAAlert.GetDisplayName: string;
begin
  Result := FName;
end;

procedure TcxDAAlert.HandleOptionsClick(Sender: TObject);
var
  APoint: TPoint;
begin
  with TcxDesktopAlert(TcxDAAlerts(Collection).Owner) do
  begin
    if Assigned(FOnOptionsClick) then
      FOnOptionsClick(Sender);
    if Assigned(FOptionsPopup) then
    begin
      APoint := Point(Form.Left + Form.OptionsButton.Left, Form.Top +
        Form.OptionsButton.Top + Form.OptionsButton.Height);
      FOptionsPopup.Popup(APoint.X, APoint.Y);
    end;
  end;
end;    

procedure TcxDAAlert.SetAction(const Value: TBasicAction);
begin
  if Value = nil then
  begin
    ActionLink.Free;
    ActionLink := nil;
  end
  else
  begin
    if ActionLink = nil then
      ActionLink := TActionLink.Create(Self);
    ActionLink.Action := Value;
    ActionLink.OnChange := DoActionChange;
    ActionChange(Value, csLoading in Value.ComponentState);
    Value.FreeNotification(FNotifier);
  end;
end;

procedure TcxDAAlert.SetButtons(const Value: TcxDAButtonSet);
begin
  if FButtons <> Value then
  begin
    FButtons := Value;
  end;
end;

procedure TcxDAAlert.SetImageIndex(Value: TImageIndex);
begin
  if FImageIndex <> Value then
  begin
    FImageIndex := Value;
  end;
end;

procedure TcxDAAlert.SetOptionsButton(const Value: Boolean);
begin
  if FOptionsButton <> Value then
  begin
    FOptionsButton := Value;
  end;
end;

procedure TcxDAAlert.SetOptionsPopup(Value: TPopupMenu);
begin
  if FOptionsPopup <> Value then
  begin
    FOptionsPopup := Value;
    if Assigned(FOptionsPopup) then
      FOptionsPopup.FreeNotification(FNotifier);
  end;
end;    

{ TcxDAAlerts }

function TcxDAAlerts.Add: TcxDAAlert;
begin
  result := inherited Add as TcxDAAlert;
end;

function TcxDAAlerts.GetItem(I: Integer): TcxDAAlert;
begin
  Result := inherited Items[I] as TcxDAAlert;
end;

{ TcxDAAlertNotifier }

procedure TcxDAAlertNotifier.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
    if AComponent = FAlert.OptionsPopup then
      FAlert.OptionsPopup := nil
    else if AComponent = FAlert.Action then
      FAlert.Action := nil;
end;

procedure TcxDAAlertNotifier.SetAlert(const Value: TcxDAAlert);
begin
  FAlert := Value;
end;         

{ TcxDAButtonNotifier }

procedure TcxDAButtonNotifier.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
    if AComponent = FButton.Action then
      FButton.Action := nil;
end;

procedure TcxDAButtonNotifier.SetButton(const Value: TcxDAButton);
begin
  FButton := Value;
end;       

{ TcxDAQueuedAlert }

constructor TcxDAQueuedAlert.Create;
begin
  FDuration := -1;
  FTransparency := -1;
end;

procedure TcxDAQueuedAlert.SetAlert(const Value: TcxDAAlert);
begin
  FAlert := Value;
end;

procedure TcxDAQueuedAlert.SetDuration(const Value: Integer);
begin
  FDuration := Value;
end;

procedure TcxDAQueuedAlert.SetMessage(const Value: string);
begin
  FMessage := Value;
end;

procedure TcxDAQueuedAlert.SetTitle(const Value: string);
begin
  FTitle := Value;
end;

procedure TcxDAQueuedAlert.SetTransparency(const Value: Integer);
begin
  FTransparency := Value;
end;

end.
