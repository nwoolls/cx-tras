// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxCB;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, cxLookAndFeels, Graphics, Windows,
    cxGraphics, Messages, cxControls;

type
  TcxPaintStyle = (psStandard, psOffice10, psOffice11, psUseLookAndFeel);

  TcxCBButtonState = (cbsNormal, cbsPressed, cbsHotTrack, cbsDisabled);

  TcxCustomCloseButton = class;

  TcxCBPainterParentInfo = class
  private
    FParentControl: TcxCustomCloseButton;
    function GetButtonHeight: Integer; virtual;
    function GetButtonState: TcxCBButtonState; virtual;
    function GetButtonWidth: Integer; virtual;
    function GetCanvas: TcxCanvas; virtual;
    function GetClientRect: TRect; virtual;
    function GetEnabled: Boolean; virtual;
    function GetTransparent: Boolean;
  protected
    property ParentControl: TcxCustomCloseButton read FParentControl;
  public
    constructor Create(AParent: TcxCustomCloseButton);
    destructor Destroy; override;
    property ButtonHeight: Integer read GetButtonHeight;
    property ButtonState: TcxCBButtonState read GetButtonState;
    property ButtonWidth: Integer read GetButtonWidth;
    property Canvas: TcxCanvas read GetCanvas;
    property ClientRect: TRect read GetClientRect;
    property Enabled: Boolean read GetEnabled;
    property Transparent: Boolean read GetTransparent;
  end;

  TcxCBCustomPainter = class
  private
    FParentControl: TcxCustomCloseButton;
    FParentInfo: TcxCBPainterParentInfo;
  protected
    function GetDisabledTextFaceColor: TColor; virtual;
    function GetDisabledTextShadowColor: TColor; virtual;
    property DisabledTextFaceColor: TColor read GetDisabledTextFaceColor;
    property DisabledTextShadowColor: TColor read GetDisabledTextShadowColor;
    property ParentControl: TcxCustomCloseButton read FParentControl;
    property ParentInfo: TcxCBPainterParentInfo read FParentInfo;
  public
    constructor Create(AParent: TcxCustomCloseButton);
    destructor Destroy; override;
    procedure Paint; virtual; abstract;
  end;

  TcxCBPainterClass = class of TcxCBCustomPainter;

  TcxCustomCloseButton = class(TcxControl, IdxSkinSupport)
  private
    FButtonState: TcxCBButtonState;
    FMouseOver: Boolean;
    FPainter: TcxCBCustomPainter;
    FPaintStyle: TcxPaintStyle;
    FTransparent: Boolean;
    function GetPaintStyle: TcxPaintStyle;
    procedure SetPaintStyle(const Value: TcxPaintStyle);
    procedure SetTransparent(const Value: Boolean);
  protected                                       
    { Protected declarations }
    function GetPainter: TcxCBCustomPainter; virtual;
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseEnter(AControl: TControl); override;
    procedure MouseLeave(AControl: TControl); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure Resize; override;
    procedure SetEnabled(Value: Boolean); override;
    property Painter: TcxCBCustomPainter read GetPainter;
  public                                 
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property ButtonState: TcxCBButtonState read FButtonState;
    property LookAndFeel;
    property PaintStyle: TcxPaintStyle read FPaintStyle write SetPaintStyle default psUseLookAndFeel;
    property Transparent: Boolean read FTransparent write SetTransparent default False;
  end;

  TcxCloseButton = class(TcxCustomCloseButton)
  published
    property Action;
    property Align;
    property Anchors;
    property Constraints;
    property Enabled;    
    property LookAndFeel;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDblClick;
    property OnMouseActivate;
    property OnMouseDown;   
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property PaintStyle;
    property ParentShowHint; 
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Transparent;
    property Visible;
  end;

implementation

uses dxThemeManager, cxCBPainters, Forms;

{ TcxCloseButton }

constructor TcxCustomCloseButton.Create(AOwner: TComponent);
begin
  inherited;
  Height := 24;
  Width := 24;
  ParentBackground := False;
  FMouseOver := False;
  FButtonState := cbsNormal;
  FPaintStyle := psUseLookAndFeel;
end;

destructor TcxCustomCloseButton.Destroy;
begin
  if Assigned(FPainter) then
    FreeAndNil(FPainter);
  inherited;
end;

function TcxCustomCloseButton.GetPainter: TcxCBCustomPainter;
var
  AClass: TcxCBPainterClass;
  AStyle: TcxPaintStyle;
begin
  if LookAndFeel.SkinName = '' then
  begin
    AStyle := GetPaintStyle;
    case AStyle of
      psOffice10: AClass := TcxCBOffice10Painter;
      psOffice11: AClass := TcxCBOffice11Painter;
    else
      AClass := TcxCBStandardPainter;
    end;
  end else
    AClass := TcxCBSkinPainter;
  if Assigned(FPainter) and not (FPainter is AClass) then
    FreeAndNil(FPainter);
  if not Assigned(FPainter) then
    FPainter := AClass.Create(Self);
  Result := FPainter;
end;

function TcxCustomCloseButton.GetPaintStyle: TcxPaintStyle;
begin
  Result := FPaintStyle;
  if Result = psUseLookAndFeel then
  begin
    if LookAndFeel.NativeStyle and AreVisualStylesAvailable then
      Result := psStandard
    else
      case LookAndFeel.Kind of
        lfFlat, lfUltraFlat: Result := psOffice10;
        lfOffice11: Result := psOffice11;
      else
        Result := psStandard;
      end;
  end;
end;

procedure TcxCustomCloseButton.LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues:
  TcxLookAndFeelValues);
begin
  inherited;
  if not (csDestroying in ComponentState) then
    Invalidate;
end;

procedure TcxCustomCloseButton.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FButtonState := cbsPressed;    
  Invalidate;
end;     

procedure TcxCustomCloseButton.MouseEnter(AControl: TControl);
begin                   
  inherited;
  if (csDesigning in ComponentState) then
    Exit;
  FMouseOver := True;
  FButtonState := cbsHotTrack;
  Invalidate;
end;

procedure TcxCustomCloseButton.MouseLeave(AControl: TControl);
begin
  inherited;
  FMouseOver := False;
  FButtonState := cbsNormal;
  Invalidate;
end;    

procedure TcxCustomCloseButton.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  if FMouseOver then
    FButtonState := cbsHotTrack
  else
    FButtonState := cbsNormal;    
  Invalidate;
end;           

procedure DrawParentImage(const AControl: TControl; const DC: HDC);
var
  SaveIndex: Integer;
  P: TPoint;
begin
  if AControl.Parent = nil then
    Exit;

  SaveIndex := SaveDC(DC);
  try
    GetViewportOrgEx(DC, P);

    SetViewportOrgEx(DC, P.X - AControl.Left, P.Y - AControl.Top, nil);
    IntersectClipRect(DC, 0, 0, AControl.Parent.ClientWidth,
      AControl.Parent.ClientHeight);

    AControl.Parent.Perform(WM_ERASEBKGND, Longint(DC), 0);
    AControl.Parent.Perform(WM_PAINT, Longint(DC), 0);
  finally
    RestoreDC(DC, SaveIndex);
  end;

  if not (AControl.Parent is TCustomControl) and
    not (AControl.Parent is TCustomForm) then
    AControl.Parent.Invalidate;
end;       

procedure TcxCustomCloseButton.Paint;
begin
  if (FTransparent or (LookAndFeel.SkinName <> '')) and not (csDesigning in ComponentState) then
    DrawParentImage(Self, Canvas.Handle);
  Painter.Paint;
end;

procedure TcxCustomCloseButton.Resize;
begin
  inherited;
  Invalidate;
end;

procedure TcxCustomCloseButton.SetEnabled(Value: Boolean);
var
  WasEnabled: Boolean;
begin
  WasEnabled := Enabled;
  inherited;
  if WasEnabled <> Enabled then
  begin
    if Enabled then
      FButtonState := cbsNormal
    else
      FButtonState := cbsDisabled;
    Invalidate;
  end;
end;       

procedure TcxCustomCloseButton.SetPaintStyle(const Value: TcxPaintStyle);
begin
  if Value <> FPaintStyle then
  begin
    FPaintStyle := Value;
    Invalidate;
  end;
end;  

procedure TcxCustomCloseButton.SetTransparent(const Value: Boolean);
begin
  if Value <> FTransparent then
  begin
    FTransparent := Value;
    Invalidate;
  end;
end;

{ TcxCBCustomPainter }

constructor TcxCBCustomPainter.Create(AParent: TcxCustomCloseButton);
begin
  inherited Create;
  FParentControl := AParent;
  FParentInfo := TcxCBPainterParentInfo.Create(AParent);
end;

destructor TcxCBCustomPainter.Destroy;
begin
  FreeAndNil(FParentInfo);
  inherited;
end;

function TcxCBCustomPainter.GetDisabledTextFaceColor: TColor;
begin
  Result := clBtnHighlight;
end;

function TcxCBCustomPainter.GetDisabledTextShadowColor: TColor;
begin
  Result := clBtnShadow;
end;              

{ TcxCBPainterParentInfo }

constructor TcxCBPainterParentInfo.Create(AParent: TcxCustomCloseButton);
begin
  inherited Create;
  FParentControl := AParent;
end;

destructor TcxCBPainterParentInfo.Destroy;
begin

  inherited;
end;

function TcxCBPainterParentInfo.GetButtonHeight: Integer;
begin
  Result := ParentControl.Height;
end;

function TcxCBPainterParentInfo.GetButtonState: TcxCBButtonState;
begin
  Result := ParentControl.ButtonState;
end;

function TcxCBPainterParentInfo.GetButtonWidth: Integer;
begin
  Result := ParentControl.Width;
end;

function TcxCBPainterParentInfo.GetCanvas: TcxCanvas;
begin
  Result := ParentControl.Canvas;
end;

function TcxCBPainterParentInfo.GetClientRect: TRect;
begin
  Result := ParentControl.ClientRect;
end;

function TcxCBPainterParentInfo.GetEnabled: Boolean;
begin
  Result := ParentControl.Enabled;
end;

function TcxCBPainterParentInfo.GetTransparent: Boolean;
begin
  Result := ParentControl.Transparent;
end;

end.
