// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxCBPainters;

interface

uses Graphics, Windows, cxCB;

type
  TcxCBCustomStandardPainter = class(TcxCBCustomPainter)
  private
    function GetButtonCenteredContentPosition: TPoint;
    procedure InternalDrawEdge(const ARect: TRect; Sunken: Boolean; ThinFrame: Boolean = False);
    procedure InternalPolyLine(const APoints: array of TPoint; AColor: TColor);
  protected
    function GetBrushColor: TColor; virtual;
    function GetButtonColor: TColor; virtual;
    function GetButtonContentColor: TColor; virtual;
    function GetButtonContentPosition: TPoint; virtual;
    function GetHotTrackColor: TColor; virtual;
    function GetPenColor: TColor; virtual;
    procedure PaintButtonBackground(const ARect: TRect); virtual;
    procedure PaintButtonContent; virtual;
    procedure PaintButtonFrame(var ARect: TRect); virtual;
    property BrushColor: TColor read GetBrushColor;
    property ButtonColor: TColor read GetButtonColor;
    property ButtonContentColor: TColor read GetButtonContentColor;
    property ButtonContentPosition: TPoint read GetButtonContentPosition;
    property HotTrackColor: TColor read GetHotTrackColor;
    property PenColor: TColor read GetPenColor;       
  public
    procedure Paint; override;
  end;

  TcxCBStandardPainter = class(TcxCBCustomStandardPainter)
  private
    function IsNativePainting: Boolean;
  public    
    procedure Paint; override;
  end;

  TcxCBOffice10Painter = class(TcxCBCustomStandardPainter)
  protected
    function GetButtonColor: TColor; override;
    function GetButtonContentColor: TColor; override;
    procedure PaintButtonFrame(var ARect: TRect); override;
  end;

  TcxCBOffice11Painter = class(TcxCBCustomStandardPainter)
  protected
    function GetButtonContentColor: TColor; override;
    procedure PaintButtonBackground(const ARect: TRect); override;
    procedure PaintButtonFrame(var ARect: TRect); override;
  end;

  TcxCBSkinPainter = class(TcxCBCustomStandardPainter)
  protected
    procedure PaintButtonBackground(const ARect: TRect); override;
    procedure PaintButtonContent; override;
    procedure PaintButtonFrame(var ARect: TRect); override;
  end;

implementation

uses Classes, cxGraphics, dxThemeManager, dxThemeConsts, dxUxTheme, dxOffice11,
  cxLookAndFeelPainters;

const
  //standard
  cxCBDarkEdgeColor = clBtnShadow;
  cxCBDarkestEdgeColor = cl3DDkShadow;
  cxCBTabBodyColor = clBtnFace;
  cxCBLightEdgeColor = cl3DLight;
  cxCBLightestEdgeColor = clBtnHighlight;
  cxCBBodyColor = clBtnFace;

  //office 10                                                  
  cxCBLightBrushColorDelta = 20;

  cxCloseButtonCrossSize = 9;

var
  IsWin98Or2000: Boolean = False;

  //office 10
  cxCBLightBrushColor: TColor;     

{ TcxCBCustomStandardPainter } 

function Size(cx, cy: Integer): TSize;
begin
  Result.cx := cx;
  Result.cy := cy;
end;

function TcxCBCustomStandardPainter.GetBrushColor: TColor;
begin
  Result := ButtonContentColor;
end;

function TcxCBCustomStandardPainter.GetButtonCenteredContentPosition: TPoint;

  function GetContentSize: TSize;
  begin
    Result := Size(cxCloseButtonCrossSize - 1, cxCloseButtonCrossSize - 2);
  end;

var
  AButtonSize, AContentSize: TSize;
begin
  AContentSize := GetContentSize;

  AButtonSize := Size(ParentInfo.ButtonWidth, ParentInfo.ButtonHeight);

  Result.X := (AButtonSize.cx - AContentSize.cx) div 2;
  Result.Y := (AButtonSize.cy - AContentSize.cy) div 2;

  if ParentInfo.ButtonState = cbsPressed then
  begin
    Inc(Result.X);
    Inc(Result.Y);
  end;
end;

function TcxCBCustomStandardPainter.GetButtonColor: TColor;
begin
  Result := cxCBBodyColor;
end;

function TcxCBCustomStandardPainter.GetButtonContentColor: TColor;
begin
  if ParentInfo.ButtonState = cbsHotTrack then
    Result := HotTrackColor
  else
    Result := clBtnText;
end;

function TcxCBCustomStandardPainter.GetButtonContentPosition: TPoint;
begin
  Result := GetButtonCenteredContentPosition;
end;

function TcxCBCustomStandardPainter.GetHotTrackColor: TColor;
begin
  if IsWin98Or2000 then
    Result := GetSysColor(COLOR_HOTLIGHT)
  else
    Result := clBlue;
end;      

function TcxCBCustomStandardPainter.GetPenColor: TColor;
begin
  Result := BrushColor;
end;

procedure TcxCBCustomStandardPainter.InternalDrawEdge(const ARect: TRect;
    Sunken: Boolean; ThinFrame: Boolean = False);
const
  ThickFrameLineColorA: array [Boolean, 1 .. 4] of TColor = (
    (cxCBLightEdgeColor, cxCBLightestEdgeColor, cxCBDarkestEdgeColor, cxCBDarkEdgeColor),
    (cxCBDarkEdgeColor, cxCBDarkestEdgeColor, cxCBLightestEdgeColor, cxCBLightEdgeColor)
  );
  ThinFrameLineColorA: array [Boolean, 1 .. 2] of TColor = (
    (cxCBLightEdgeColor, cxCBDarkestEdgeColor),
    (cxCBDarkestEdgeColor, cxCBLightEdgeColor)
  );
begin
  with ARect do
    if ThinFrame then
    begin
      InternalPolyLine([Point(Left, Bottom - 2), Point(Left, Top), Point(Right - 2, Top)], ThinFrameLineColorA[Sunken, 1]);
      InternalPolyLine([Point(Left, Bottom - 1), Point(Right - 1, Bottom - 1), Point(Right - 1, Top)], ThinFrameLineColorA[Sunken, 2]);
    end else
    begin
      InternalPolyLine([Point(Left, Bottom - 2), Point(Left, Top), Point(Right - 2, Top)], ThickFrameLineColorA[Sunken, 1]);
      InternalPolyLine([Point(Left + 1, Bottom - 3), Point(Left + 1, Top + 1), Point(Right - 3, Top + 1)], ThickFrameLineColorA[Sunken, 2]);
      InternalPolyLine([Point(Left, Bottom - 1), Point(Right - 1, Bottom - 1), Point(Right - 1, Top)], ThickFrameLineColorA[Sunken, 3]);
      InternalPolyLine([Point(Left + 1, Bottom - 2), Point(Right - 2, Bottom - 2), Point(Right - 2, Top + 1)], ThickFrameLineColorA[Sunken, 4]);
    end;
end;

procedure TcxCBCustomStandardPainter.InternalPolyLine(const APoints: array of TPoint;
  AColor: TColor);
var
  ALastPoint: TPoint;
begin
  with ParentInfo.Canvas do
  begin
    Pen.Color := AColor;
    Polyline(APoints);
    ALastPoint := APoints[High(APoints)];
    Polyline([ALastPoint, Point(ALastPoint.X + 1, ALastPoint.Y + 1)]);
  end;
end;

procedure TcxCBCustomStandardPainter.Paint;
var
  ARect: TRect;
begin            
  ARect := ParentInfo.ClientRect;
  PaintButtonFrame(ARect);
  PaintButtonBackground(ARect);
  PaintButtonContent;
end;   

procedure TcxCBCustomStandardPainter.PaintButtonBackground(const ARect: TRect);
begin
  if not ((ParentInfo.ButtonState = cbsNormal) and ParentInfo.Transparent) or
    (csDesigning in ParentControl.ComponentState) then
  begin
    ParentInfo.Canvas.Brush.Color := ButtonColor;
    ParentInfo.Canvas.FillRect(ARect);
  end;
end;

procedure TcxCBCustomStandardPainter.PaintButtonContent;

  procedure DrawCross(const ALeftTopCorner: TPoint);
  begin
    with ALeftTopCorner do
    begin
      InternalPolyLine([Point(X + 1, Y), Point(X + cxCloseButtonCrossSize - 2,
        Y + cxCloseButtonCrossSize - 3)], ParentInfo.Canvas.Pen.Color);
      InternalPolyLine([Point(X, Y), Point(X + cxCloseButtonCrossSize - 3,
        Y + cxCloseButtonCrossSize - 3)], ParentInfo.Canvas.Pen.Color);

      InternalPolyLine([Point(X, Y + cxCloseButtonCrossSize - 3),
        Point(X + cxCloseButtonCrossSize - 3, Y)], ParentInfo.Canvas.Pen.Color);
      InternalPolyLine([Point(X + 1, Y + cxCloseButtonCrossSize - 3),
        Point(X + cxCloseButtonCrossSize - 2, Y)], ParentInfo.Canvas.Pen.Color);
    end;
  end;

begin
  with ParentInfo.Canvas do
  begin
    if not ParentInfo.Enabled then
    begin
      Brush.Color := DisabledTextFaceColor;
      Pen.Color := DisabledTextFaceColor;
      with ButtonContentPosition do
        DrawCross(Point(X + 1, Y + 1));
      Brush.Color := DisabledTextShadowColor;
      Pen.Color := DisabledTextShadowColor;
    end else
    begin
      Brush.Color := BrushColor;
      Pen.Color := PenColor;
    end;
    DrawCross(ButtonContentPosition)
  end;
  ParentInfo.Canvas.Brush.Style := bsSolid;
end;      

procedure TcxCBCustomStandardPainter.PaintButtonFrame(var ARect: TRect);
begin
  if not ((ParentInfo.ButtonState = cbsNormal) and ParentInfo.Transparent) or
    (csDesigning in ParentControl.ComponentState) then
  begin
    InternalDrawEdge(ARect, ParentInfo.ButtonState = cbsPressed);
    InflateRect(ARect, -2, -2)
  end;
end;

procedure RetrieveWindowsVersion;
var
  Info: TOSVersionInfo;
begin
  Info.dwOSVersionInfoSize := SizeOf(Info);
  GetVersionEx(Info);
  IsWin98Or2000 :=
    (Info.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS) and (Info.dwMinorVersion <> 0) or
    (Info.dwPlatformId = VER_PLATFORM_WIN32_NT) and (Info.dwMajorVersion = 5);
end;

{ TcxCBStandardPainter }  

function TcxCBStandardPainter.IsNativePainting: Boolean;
begin
  if csDestroying in ParentControl.ComponentState then
    Result := False
  else
    Result := AreVisualStylesAvailable and (OpenTheme(totWindow) <> TC_NONE);
end;

procedure TcxCBStandardPainter.Paint;
const
  ACloseButtonStateIdMap: array [TcxCBButtonState] of Integer =
    (CBS_NORMAL, CBS_PUSHED, CBS_HOT, CBS_DISABLED);
var
  APartId, AStateId: Integer;
  ATheme: TdxTheme;
  AButtonRect: TRect;
begin
  if IsNativePainting then
  begin
    ATheme := OpenTheme(totWindow);
    APartId := WP_SMALLCLOSEBUTTON;
    AStateId := ACloseButtonStateIdMap[ParentInfo.ButtonState];

    AButtonRect := ParentInfo.ClientRect;
    if IsThemeBackgroundPartiallyTransparent(ATheme, APartId, AStateId) then
      DrawThemeParentBackground(ParentControl.Handle, ParentInfo.Canvas.Handle,
        AButtonRect);
    DrawThemeBackground(ATheme, ParentInfo.Canvas.Handle, APartId, AStateId,
      AButtonRect);
  end else
    inherited;
end;       

{ TcxCBOffice10Painter }

function TcxCBOffice10Painter.GetButtonColor: TColor;
begin
  if ParentInfo.ButtonState = cbsHotTrack then
    Result := clBtnFace
  else
    if ParentInfo.ButtonState = cbsPressed then
      Result := clBtnShadow
    else
      Result := cxCBLightBrushColor;
end;

function TcxCBOffice10Painter.GetButtonContentColor: TColor;
begin
  if not ParentControl.Enabled or (ParentInfo.ButtonState = cbsDisabled) then
    Result := cxCBDarkEdgeColor
  else
    if ParentInfo.ButtonState = cbsPressed then
      Result := cxCBLightestEdgeColor
    else
      Result := clBtnText;
end;       

procedure TcxCBOffice10Painter.PaintButtonFrame(var ARect: TRect);

  function GetFrameColor: TColor;
  begin
    if ParentInfo.ButtonState in [cbsNormal, cbsDisabled] then
      Result := clBtnShadow
    else
      Result := clBlack;
  end;

begin
  if not ((ParentInfo.ButtonState = cbsNormal) and ParentInfo.Transparent) or
    (csDesigning in ParentControl.ComponentState) then
  begin
    ParentInfo.Canvas.DrawComplexFrame(ParentInfo.ClientRect,
      GetFrameColor, GetFrameColor);
    InflateRect(ARect, -1, -1);
  end;
end;

procedure CalculateLightBrushColor;
var
  R, G, B: Integer;
  Color: Integer;
begin
  Color := ColorToRGB(clBtnFace);
  R := GetRValue(Color) + cxCBLightBrushColorDelta;
  if R > 255 then R := 255;
  G := GetGValue(Color) + cxCBLightBrushColorDelta;
  if G > 255 then G := 255;
  B := GetBValue(Color) + cxCBLightBrushColorDelta;
  if B > 255 then B := 255;
  cxCBLightBrushColor := RGB(R, G, B);
end;

{ TcxCBOffice11Painter }

function TcxCBOffice11Painter.GetButtonContentColor: TColor;
begin         
  if ParentInfo.ButtonState = cbsDisabled then
    Result := dxOffice11TextDisabledColor
  else
    Result := dxOffice11TextEnabledColor;
end;

procedure TcxCBOffice11Painter.PaintButtonBackground(const ARect: TRect);
var
  AColor1, AColor2: TColor;
begin         
  if not ((ParentInfo.ButtonState = cbsNormal) and ParentInfo.Transparent) or
    (csDesigning in ParentControl.ComponentState) then
  begin
    AColor1 := dxOffice11NavPaneGroupCaptionColor1;
    AColor2 := dxOffice11NavPaneGroupCaptionColor1;
    case ParentInfo.ButtonState of
      cbsPressed, cbsHotTrack:
        begin
          if ParentInfo.ButtonState = cbsHotTrack then
          begin
            AColor1 := dxOffice11NavPaneGroupCaptionPressedColor1;
            AColor2 := dxOffice11NavPaneGroupCaptionPressedColor1;
          end
          else
          begin
            AColor1 := dxOffice11SelectedDownColor1;
            AColor2 := dxOffice11SelectedDownColor1;
          end;
        end;
    end;
    FillGradientRect(ParentInfo.Canvas.Handle, ARect, AColor1,
      AColor2, False);
  end;
end;

procedure TcxCBOffice11Painter.PaintButtonFrame(var ARect: TRect);

  function GetFrameColor: TColor;
  begin
    if not ParentInfo.Enabled or (ParentInfo.ButtonState = cbsDisabled) then
      Result := dxOffice11TextDisabledColor
    else
      Result := dxOffice11NavPaneBorder;
  end;

begin
  if not ((ParentInfo.ButtonState = cbsNormal) and ParentInfo.Transparent) or
    (csDesigning in ParentControl.ComponentState) then
  begin
    ParentInfo.Canvas.FrameRect(ARect, GetFrameColor);
    InflateRect(ARect, -1, -1);
  end;
end;

{ TcxCBSkinPainter }

procedure TcxCBSkinPainter.PaintButtonBackground(const ARect: TRect);
var
  State: TcxButtonState;
begin
  State := cxbsDefault;
  case ParentInfo.ButtonState of                                                 
    cbsNormal: State := cxbsNormal;
    cbsHotTrack: State := cxbsHot;
    cbsPressed: State := cxbsPressed;
  end;
  if not ParentInfo.Enabled then
    State := cxbsDisabled;
  ParentControl.LookAndFeel.Painter.DrawFilterCloseButton(ParentInfo.Canvas, ParentInfo.ClientRect, State);
end;

procedure TcxCBSkinPainter.PaintButtonContent;
begin

end;

procedure TcxCBSkinPainter.PaintButtonFrame(var ARect: TRect);
begin

end;

initialization
  RetrieveWindowsVersion;    
  CalculateLightBrushColor;

end.
