// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxSG;

interface

uses
  SysUtils, Classes, Forms, Controls, Messages, Windows, Graphics;

type
  TcxSizeGrip = class(TGraphicControl)
  private
    FResizingControl  : TWinControl;
    procedure DrawThemedSizeGrip;              
    procedure DrawUnthemedSizeGrip;
    function GetResizingControl: TWinControl;
    procedure SetResizingControl(const Value: TWinControl);
  protected
    procedure ClickGrip(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Paint; override;
    procedure SetParent(AParent: TWinControl); override;
    procedure WMMouseActivate(var Message: TMessage); message WM_MOUSEACTIVATE;
  public
    constructor Create(AOwner: TComponent); override;
    procedure BeginResize;
    procedure Loaded; override;
    procedure PlaceOnControl(const Control: TWinControl);
    property ResizingControl: TWinControl read GetResizingControl write SetResizingControl;
  end;

implementation

uses dxThemeManager, dxThemeConsts, dxUxTheme;

constructor TcxSizeGrip.Create(AOwner: TComponent);
var
  Offset: Integer;
begin
  inherited;
  Offset := GetSystemMetrics(SM_CXVSCROLL);
  Width := Offset;
  Height := Offset;
  OnMouseDown := ClickGrip;
  Cursor := crSizeNWSE;
end;

procedure TcxSizeGrip.BeginResize;
begin
  if Assigned(ResizingControl) then
  begin
    ReleaseCapture;
    ResizingControl.Perform(WM_SYSCOMMAND, $F008, 0);
  end;
end;

procedure TcxSizeGrip.ClickGrip(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  BeginResize;
end;

procedure TcxSizeGrip.DrawThemedSizeGrip;
var
  ATheme: TdxTheme;
  ARect: TRect;
begin
  ATheme := OpenTheme(totStatus);
  ARect := ClientRect;
  DrawThemeBackground(ATheme, Canvas.Handle, SP_GRIPPER, 0, @ARect);
end;

function TcxSizeGrip.GetResizingControl: TWinControl;
begin
  if not Assigned(FResizingControl) then
  begin
    FResizingControl := Parent;
    while Assigned(FResizingControl) do
    begin
      if (FResizingControl is TCustomForm) then
      begin
        Break;
      end else
        FResizingControl := FResizingControl.Parent;
    end;
  end;

  if Assigned(FResizingControl) then
    Result := FResizingControl
  else
    Result := nil;
end;

procedure TcxSizeGrip.Loaded;
begin
  inherited;
  PlaceOnControl(Parent);
end;

procedure DrawSizeGrip(DC: HDC; R: TRect); // dxBar
const
  ROP_DSPDxax = $00E20746;
var
  APrevBitmap, ATempBitmap, AMaskBitmap: HBITMAP;
  TempDC, MDC, MaskDC: HDC;
  W, H: Integer;
  APrevBkColor: COLORREF;
begin
  W := R.Right - R.Left;
  H := R.Bottom - R.Top;
  TempDC := CreateCompatibleDC(DC);
  ATempBitmap := SelectObject(TempDC, CreateCompatibleBitmap(DC, W, H));
  try
    BitBlt(TempDC, 0, 0, W, H, DC, R.Left, R.Top, SRCCOPY); // 1
    MDC := CreateCompatibleDC(DC);
    APrevBitmap := SelectObject(MDC, CreateCompatibleBitmap(DC, W, H));
    DrawFrameControl(MDC, Rect(0, 0, W, H), DFC_SCROLL, DFCS_SCROLLSIZEGRIP); // 2

    MaskDC := CreateCompatibleDC(DC);
    AMaskBitmap := SelectObject(MaskDC, CreateBitmap(W, H, 1, 1, nil));
    try
      APrevBkColor := SetBkColor(MDC, ColorToRGB(clBtnFace)); //!
      BitBlt(MaskDC, 0, 0, W, H, MDC, 0, 0, SRCCOPY);
      SetBkColor(MDC, APrevBkColor);

      BitBlt(TempDC, 0, 0, W, H, MaskDC, 0, 0, MERGEPAINT);
      BitBlt(MDC, 0, 0, W, H, MaskDC, 0, 0, SRCPAINT);
      BitBlt(TempDC, 0, 0, W, H, MDC, 0, 0, SRCAND);
    finally
      DeleteObject(SelectObject(MaskDC, AMaskBitmap));
      DeleteDC(MaskDC);
    end;

    DeleteObject(SelectObject(MDC, APrevBitmap));
    DeleteDC(MDC);

    BitBlt(DC, R.Left, R.Top, W, H, TempDC, 0, 0, SRCCOPY);
  finally
    DeleteObject(SelectObject(TempDC, ATempBitmap));
    DeleteDC(TempDC);
  end;
end;

procedure TcxSizeGrip.DrawUnthemedSizeGrip;
begin
  DrawSizeGrip(Canvas.Handle, ClientRect);
end;

procedure TcxSizeGrip.Paint;
begin
  inherited;     
  if not Assigned(ResizingControl) then
    Exit;

  if ((FResizingControl is TCustomForm) and
    (TCustomForm(FResizingControl).WindowState <> wsMaximized)) or
    not (FResizingControl is TCustomForm) then
  begin
    if AreVisualStylesAvailable then
      DrawThemedSizeGrip
    else
      DrawUnthemedSizeGrip;
  end;
end;

procedure TcxSizeGrip.PlaceOnControl(const Control: TWinControl);
var
  Offset: Integer;
begin
  if Control = nil then
    Exit;
  if Parent <> Control then
    Parent := Control;
  Offset := GetSystemMetrics(SM_CXVSCROLL);
  Align := alNone;
  SetBounds(Control.ClientWidth - Offset, Control.ClientHeight - Offset,
    Offset, Offset);
  Anchors := [akRight, akBottom];
end;

procedure TcxSizeGrip.SetParent(AParent: TWinControl);
begin
  inherited;
  FResizingControl := nil;
  PlaceOnControl(AParent);
end;

procedure TcxSizeGrip.SetResizingControl(const Value: TWinControl);
begin
  FResizingControl := Value;
end;

procedure TcxSizeGrip.WMMouseActivate(var Message: TMessage);
begin
  Message.Result := MA_NOACTIVATE;
end;

end.
