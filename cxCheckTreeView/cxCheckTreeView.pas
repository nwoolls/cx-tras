// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxCheckTreeView;

interface

uses
  SysUtils, Classes, Controls, cxControls, cxContainer, cxTreeView,
  cxLookAndFeels, cxEdit, Windows, ComCtrls, cxLookAndFeelPainters;

type             
  TcxCheckTreeViewChangingEvent = procedure(Sender: TObject; ANode: TTreeNode; ACheckState: TcxCheckBoxState; var AllowChange: Boolean) of object;

  TcxCheckTreeViewChangeEvent = procedure(Sender: TObject; ANode: TTreeNode; ACheckState: TcxCheckBoxState) of object;

  TcxCheckTreeViewCascadeCompleteEvent = procedure(Sender: TObject; ANode: TTreeNode) of object;

  TcxCheckTreeView = class(TcxTreeView, IdxSkinSupport)
  private
    { Private declarations }
    FCascadeChecks: Boolean;
    FCascadeCount: Integer;
    FCheckImages: TImageList;    
    FClickedItem: TTreeNode;
    FOnCascadeComplete: TcxCheckTreeViewCascadeCompleteEvent;
    FOnStateChange: TcxCheckTreeViewChangeEvent;
    FOnStateChanging: TcxCheckTreeViewChangingEvent;
    procedure CascadeChildren(const ANode: TTreeNode);
    procedure CascadeParent(const ANode: TTreeNode);
    procedure CheckSiblingsAndChildren(ANode: TTreeNode; ACheckState: TcxCheckBoxState);
    function CheckStateToIndex(const ACheckState: TcxCheckBoxState): Integer;
    function GetCheckBoxSize: TSize;
    procedure GetCheckImages;
    function GetEditBorderStyle: TcxEditBorderStyle;
    function IndexToCheckState(const Index: Integer): TcxCheckBoxState;
    procedure SetCascadeChecks(const Value: Boolean);
  protected
    { Protected declarations }
    function CanChangeState(const ANode: TTreeNode; ACheckState: TcxCheckBoxState): Boolean; virtual;
    procedure DoEnter; override;
    procedure DoExit; override;   
    procedure EnabledChanged; override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure StateChanged(const ANode: TTreeNode; ACheckState: TcxCheckBoxState); virtual;
  public
    { Public declarations }                       
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CheckAllNodes(const ACheckState: TcxCheckBoxState);
    function GetCheckState(const ANode: TTreeNode): TcxCheckBoxState;
    function HasCheckedNodes: Boolean;
    procedure Loaded; override;
    procedure SetCheckState(const ANode: TTreeNode; ACheckState: TcxCheckBoxState);
    procedure ToggleCheckState(const ANode: TTreeNode);
    property CheckImages: TImageList read FCheckImages;
  published
    { Published declarations }
    property CascadeChecks: Boolean read FCascadeChecks write SetCascadeChecks default True;
    property OnCascadeComplete: TcxCheckTreeViewCascadeCompleteEvent read FOnCascadeComplete write FOnCascadeComplete;
    property OnStateChange: TcxCheckTreeViewChangeEvent read FOnStateChange write FOnStateChange;
    property OnStateChanging: TcxCheckTreeViewChangingEvent read FOnStateChanging write FOnStateChanging;
  end;

procedure Register;

implementation      

uses cxGraphics, dxUxTheme, dxThemeConsts, dxThemeManager, Graphics, Math,
  cxCheckBox;

procedure Register;
begin
  RegisterComponents('cx-tras', [TcxCheckTreeView]);
end;        

{ TcxCheckTreeView }

constructor TcxCheckTreeView.Create(AOwner: TComponent);
begin
  inherited;
  FCascadeCount := 0;
  FClickedItem := nil;
  FCascadeChecks := True;
  FCheckImages := TImageList.Create(Self);
  FCheckImages.Name := 'CheckImages';
  StateImages := CheckImages;    
  GetCheckImages;
end;

destructor TcxCheckTreeView.Destroy;
begin
  FreeAndNil(FCheckImages);
  inherited;
end;

function TcxCheckTreeView.CanChangeState(const ANode: TTreeNode;
  ACheckState: TcxCheckBoxState): Boolean;
begin
  Result := True;
  if Assigned(FOnStateChanging) then
    FOnStateChanging(Self, ANode, ACheckState, Result);
end;

procedure TcxCheckTreeView.CascadeChildren(const ANode: TTreeNode);
begin
  if Assigned(ANode) and CascadeChecks and (ANode.Count > 0) and
    (IndexToCheckState(ANode.StateIndex) <> cbsGrayed) then
    CheckSiblingsAndChildren(ANode.getFirstChild,
      IndexToCheckState(ANode.StateIndex));
end;

procedure TcxCheckTreeView.CascadeParent(const ANode: TTreeNode);
var
  Node: TTreeNode;
  AllChecked: Boolean;
  AllUnchecked: Boolean;
begin
  if Assigned(ANode) and CascadeChecks and Assigned(ANode.Parent) then
  begin
    Node := ANode.Parent.getFirstChild;
    AllChecked := True;
    AllUnchecked := True;
    while Assigned(Node) do
    begin
      case IndexToCheckState(Node.StateIndex) of
        cbsChecked: AllUnchecked := False;
        cbsUnchecked: AllChecked := False;
      else
        AllChecked := False;
        AllUnchecked := False;
      end;
      Node := Node.getNextSibling;
    end;
    if AllChecked then
      SetCheckState(ANode.Parent, cbsChecked)
    else if AllUnchecked then
      SetCheckState(ANode.Parent, cbsUnchecked)
    else
      SetCheckState(ANode.Parent, cbsGrayed);
    CascadeParent(ANode.Parent);   
  end;
end;

procedure TcxCheckTreeView.CheckAllNodes(const ACheckState: TcxCheckBoxState);
begin   
  CheckSiblingsAndChildren(Items[0], ACheckState);
end;

procedure TcxCheckTreeView.CheckSiblingsAndChildren(ANode: TTreeNode;
  ACheckState: TcxCheckBoxState);
var
  Node: TTreeNode;
begin
  Node := ANode;
  while Assigned(Node) do
  begin
    SetCheckState(Node, ACheckState);
    if Node.Count > 0 then
      CheckSiblingsAndChildren(Node.getFirstChild, ACheckState);
    Node := Node.GetNextSibling;
  end;
end;

function TcxCheckTreeView.CheckStateToIndex(
  const ACheckState: TcxCheckBoxState): Integer;
begin
  case ACheckState of
    cbsUnchecked: Result := 2;
    cbsChecked: Result := 3;
  else
    Result := 1;
  end;
end;

procedure TcxCheckTreeView.DoEnter;
begin
  inherited;
  GetCheckImages;
end;

procedure TcxCheckTreeView.DoExit;
begin
  inherited;
  GetCheckImages;
end;

procedure TcxCheckTreeView.EnabledChanged;
begin
  inherited;
  GetCheckImages;
end;

function TcxCheckTreeView.GetCheckBoxSize: TSize;
const
  CheckSize = 13;
var
  ATheme: TdxTheme;
begin
  if AreVisualStylesMustBeUsed(RootLookAndFeel.NativeStyle, totButton) and
    HandleAllocated then
  begin
    ATheme := OpenTheme(totButton);
    GetThemePartSize(ATheme, Canvas.Handle, BP_CHECKBOX, CBS_CHECKEDNORMAL,
      TS_TRUE, Result);
  end else
  begin
    Result.cx := CheckSize;
    Result.cy := CheckSize;
  end;
end;

procedure TcxCheckTreeView.GetCheckImages;
var
  ACanvas: TcxCanvas;
  Bitmap: TBitmap;
  ABorderStyle: TcxEditBorderStyle;
  CheckSize: TSize;
  procedure AddEmptyImage;
  begin
    Bitmap := TBitmap.Create;
    try
      Bitmap.Height := CheckSize.cy;
      Bitmap.Width := CheckSize.cx;
      StateImages.Add(Bitmap, nil);
    finally
      FreeAndNil(Bitmap);
    end;
  end;

  procedure AddCheckImage(const ACheckState: TcxCheckBoxState);
  var
    EditState: TcxEditCheckState;
  begin
    Bitmap := TBitmap.Create;
    try
      Bitmap.Height := CheckSize.cy;
      Bitmap.Width := CheckSize.cx;

      if not Enabled then
        EditState := ecsDisabled
      else
        EditState := ecsNormal;                 

      ACanvas := TcxCanvas.Create(Bitmap.Canvas);
      try
        DrawEditCheck(ACanvas, Rect(0, 0, CheckSize.cx, CheckSize.cy),
          ACheckState, EditState, nil, -1, ABorderStyle,             
          AreVisualStylesMustBeUsed(Style.LookAndFeel.NativeStyle, totButton),
          clBtnText, clWindow, False, csDesigning in ComponentState,
          Focused, True, LookAndFeel.SkinPainter);

        StateImages.Add(Bitmap, nil);
      finally
        FreeAndNil(ACanvas);
      end;
    finally
      FreeAndNil(Bitmap);
    end;
  end;
begin
  if (csDestroying in ComponentState) or not Assigned(InnerTreeView) or
    not Assigned(InnerTreeView.StateImages) then
    Exit;
  CheckSize := GetCheckBoxSize;       
  StateImages.Height := CheckSize.cy;
  StateImages.Width := CheckSize.cx;
  StateImages.Clear;

  ABorderStyle := GetEditBorderStyle;

  AddEmptyImage;
  AddCheckImage(cbsGrayed);
  AddCheckImage(cbsUnchecked);
  AddCheckImage(cbsChecked);
end;

function TcxCheckTreeView.GetCheckState(
  const ANode: TTreeNode): TcxCheckBoxState;
begin
  Result := IndexToCheckState(ANode.StateIndex);
end;

function TcxCheckTreeView.GetEditBorderStyle: TcxEditBorderStyle;
begin
  case Style.LookAndFeel.Kind of
    lfFlat: Result := ebsFlat;
    lfStandard: Result := ebs3D;
    lfUltraFlat: Result := ebsUltraFlat;
    lfOffice11: Result := ebsOffice11;
  else
    Result := ebs3D;
  end;
end;

function TcxCheckTreeView.HasCheckedNodes: Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Items.Count - 1 do
  begin
    if GetCheckState(Items[I]) = cbsChecked then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TcxCheckTreeView.IndexToCheckState(
  const Index: Integer): TcxCheckBoxState;
begin
  case Index of
    2: Result := cbsUnchecked;
    3: Result := cbsChecked;
  else
    Result := cbsGrayed;
  end;
end;

procedure TcxCheckTreeView.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Assigned(Selected) and (Key = VK_SPACE) and not IsEditing then
    ToggleCheckState(Selected);
end;

procedure TcxCheckTreeView.Loaded;
begin
  inherited;     
end;

procedure TcxCheckTreeView.LookAndFeelChanged(Sender: TcxLookAndFeel;
  AChangedValues: TcxLookAndFeelValues);
begin   
  inherited;
  GetCheckImages;
end;

procedure TcxCheckTreeView.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  FClickedItem := GetNodeAt(X, Y);
end;

procedure TcxCheckTreeView.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var
  HitTest: THitTests;
  Allow: Boolean;
begin
  inherited;
  HitTest := GetHitTestInfoAt(X, Y);
  if (htOnStateIcon in HitTest) and Assigned(FClickedItem) and
    (FClickedItem = GetNodeAt(X, Y)) then
  begin
    Allow := True;
    if Assigned(OnChanging) then
      OnChanging(Self, GetNodeAt(X, Y), Allow);
    if Allow then
      ToggleCheckState(GetNodeAt(X, Y));
    FClickedItem := nil;
  end;
end;

procedure TcxCheckTreeView.Paint;
var
  I: Integer;
begin
  inherited;
  for I := 0 to Items.Count - 1 do
  begin
    if Items[I].StateIndex = -1 then
      Items[I].StateIndex := CheckStateToIndex(cbsUnchecked);
  end;
end;

procedure TcxCheckTreeView.SetCascadeChecks(const Value: Boolean);
begin
  FCascadeChecks := Value;
end;

procedure TcxCheckTreeView.SetCheckState(const ANode: TTreeNode;
  ACheckState: TcxCheckBoxState);
begin
  if (ACheckState <> IndexToCheckState(ANode.StateIndex)) and
    CanChangeState(ANode, ACheckState) then
  begin
    ANode.StateIndex := CheckStateToIndex(ACheckState);
    StateChanged(ANode, ACheckState); 
    if CascadeChecks and (FCascadeCount = 0) then
    begin
      Inc(FCascadeCount);
      Items.BeginUpdate;
      try
        CascadeChildren(ANode);
        CascadeParent(ANode);
      finally
        Items.EndUpdate;
        Dec(FCascadeCount);
        FCascadeCount := Max(FCascadeCount, 0);
      end;
    end;
    if (FCascadeCount = 0) and Assigned(FOnCascadeComplete) then
      FOnCascadeComplete(Self, ANode);
  end;
end;

procedure TcxCheckTreeView.StateChanged(const ANode: TTreeNode;
  ACheckState: TcxCheckBoxState);
begin
  if Assigned(FOnStateChange) then
    FOnStateChange(Self, ANode, ACheckState);
end;

procedure TcxCheckTreeView.ToggleCheckState(const ANode: TTreeNode);
var
  ACheckState: TcxCheckBoxState;
begin
  ACheckState := GetCheckState(ANode);
  if ACheckState = cbsChecked then
    ACheckState := cbsUnchecked
  else
    ACheckState := cbsChecked;
  SetCheckState(ANode, ACheckState);
end;

end.
