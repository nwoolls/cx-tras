// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxEB;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, cxLookAndFeels, Graphics, Windows,
    cxGraphics, Messages, cxControls, cxLookAndFeelPainters;

type                           
  TcxPaintStyle = (psStandard, psOffice10, psOffice11, psUseLookAndFeel);

  TcxCustomExpandButton = class(TcxControl, IdxSkinSupport)
  private
    FExpanded: Boolean;
    FOnCollapse: TNotifyEvent;
    FOnExpand: TNotifyEvent;
    FPaintStyle: TcxPaintStyle;
    procedure SetExpanded(const Value: Boolean);
    procedure SetPaintStyle(const Value: TcxPaintStyle);
  protected                  
    { Protected declarations }
    procedure AdjustSize; override;
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure Resize; override;
  public                               
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Expanded: Boolean read FExpanded write SetExpanded default True;
    property LookAndFeel;
    property PaintStyle: TcxPaintStyle read FPaintStyle write SetPaintStyle default psUseLookAndFeel;
    property OnCollapse: TNotifyEvent read FOnCollapse write FOnCollapse;
    property OnExpand: TNotifyEvent read FOnExpand write FOnExpand;
  end;

  TcxExpandButton = class(TcxCustomExpandButton)
  published
    property Action;
    property Align;
    property Anchors;
    property Constraints;
    property Enabled;
    property Expanded;
    property LookAndFeel;
    property OnCanResize;
    property OnClick;
    property OnCollapse;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDblClick;
    property OnExpand;
    property OnMouseActivate;
    property OnMouseDown;   
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property PaintStyle;
    property ParentShowHint; 
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
  end;

implementation

uses dxThemeManager, Forms;   

{ TcxExpandButton }

constructor TcxCustomExpandButton.Create(AOwner: TComponent);
begin
  inherited;
  AdjustSize;
  ParentBackground := False;
  FExpanded := True;
  FPaintStyle := psUseLookAndFeel;
end;

destructor TcxCustomExpandButton.Destroy;
begin
  inherited;
end;

procedure TcxCustomExpandButton.AdjustSize;
begin
  inherited;
  Height := LookAndFeel.Painter.SmallExpandButtonSize;
  Width := LookAndFeel.Painter.SmallExpandButtonSize;
end;

procedure TcxCustomExpandButton.LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues:
  TcxLookAndFeelValues);
begin
  inherited;
  if not (csDestroying in ComponentState) then
    Invalidate;
end;

procedure TcxCustomExpandButton.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if Enabled then
  begin
    Expanded := not FExpanded;
  end;
  Invalidate;
end;         

procedure DrawParentImage(const AControl: TControl; const DC: HDC);
var
  SaveIndex: Integer;
  P: TPoint;
begin
  if AControl.Parent = nil then
    Exit;

  SaveIndex := SaveDC(DC);
  try
    GetViewportOrgEx(DC, P);

    SetViewportOrgEx(DC, P.X - AControl.Left, P.Y - AControl.Top, nil);
    IntersectClipRect(DC, 0, 0, AControl.Parent.ClientWidth,
      AControl.Parent.ClientHeight);

    AControl.Parent.Perform(WM_ERASEBKGND, Longint(DC), 0);
    AControl.Parent.Perform(WM_PAINT, Longint(DC), 0);
  finally
    RestoreDC(DC, SaveIndex);
  end;

  if not (AControl.Parent is TCustomControl) and
    not (AControl.Parent is TCustomForm) then
    AControl.Parent.Invalidate;
end;

procedure TcxCustomExpandButton.Paint;
var
  R: TRect;
begin
  inherited;
  Canvas.Brush.Color := clBtnFace;
  Canvas.FillRect(ClientRect);

  AdjustSize;
                                                           
  R.Top := 0;                                                    
  R.Left := 0;
  R.Right := Width;
  R.Bottom := Height;
  
  if not (csDesigning in ComponentState) then
    DrawParentImage(Self, Canvas.Handle);

  LookAndFeel.Painter.DrawSmallExpandButton(Canvas, R, Expanded, clBtnShadow);
end;

procedure TcxCustomExpandButton.Resize;
begin
  inherited;
  Invalidate;
end;

procedure TcxCustomExpandButton.SetExpanded(const Value: Boolean);
begin
  if FExpanded <> Value then
  begin
    FExpanded := Value;
    Invalidate;
    if FExpanded and Assigned(FOnExpand) then
      FOnExpand(Self)
    else if not FExpanded and Assigned(FOnCollapse) then
      FOnCollapse(Self);
  end;
end;

procedure TcxCustomExpandButton.SetPaintStyle(const Value: TcxPaintStyle);
begin
  if Value <> FPaintStyle then
  begin
    FPaintStyle := Value;
    Invalidate;
  end;
end;

end.
