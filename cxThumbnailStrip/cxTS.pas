// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxTS;

{$R cxThumbnailStrip_Glyphs.res}

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, cxOG, Contnrs, cxLookAndFeels;

type
  TcxStripOrientation = (soUnknown, soHorizontal, soVertical);

  TcxCustomThumbnailStrip = class(TcxCustomOfficeGizmo, IdxSkinSupport)
  private
    { Private declarations }
    FBackButton: TcxOfficeGizmo;
    FBackButtonDown : Boolean;
    FFiles: TStrings;
    FForwardButton: TcxOfficeGizmo;
    FForwardButtonDown: Boolean;
    FGradient: Boolean;
    FHolderPanel: TcxOfficeGizmo;
    FImageLength: Integer;
    FItemIndex: Integer;
    FList: TObjectList;
    FLockCount: Integer;
    FOldHeight: Integer;
    FOldWidth: Integer;
    FOnImageClick: TNotifyEvent;
    FOrientation: TcxStripOrientation;
    FPosition: Integer;
    FRecreate: Boolean;
    procedure HandleBackButtonDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure HandleBackButtonUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure HandleFilesChange(Sender: TObject);
    procedure HandleForwardButtonDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure HandleForwardButtonUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RecreateImages;
    procedure SetFiles(const Value: TStrings);
    procedure SetGradient(const Value: Boolean);
    procedure SetImageLength(const Value: Integer);
    procedure SetItemIndex(const Value: Integer);
    procedure SetOnImageClick(const Value: TNotifyEvent);
    procedure SetOrientation(const Value: TcxStripOrientation);
    procedure SetPosition(const Value: Integer);
    procedure UpdateButtons;
  protected                        
    { Protected declarations }
    procedure DoImageClick(Sender: TObject); virtual;
    procedure DoImageListChange; override;
    procedure HandleImageClick(Sender: TObject);
    procedure Resize; override;
    property BackButton: TcxOfficeGizmo read FBackButton;
    property ForwardButton: TcxOfficeGizmo read FForwardButton;
    property HolderPanel: TcxOfficeGizmo read FHolderPanel;
    property ImageLength: Integer read FImageLength write SetImageLength;
    property ItemIndex: Integer read FItemIndex write SetItemIndex default -1;
    property List: TObjectList read FList;
    property Orientation: TcxStripOrientation read FOrientation write SetOrientation;
    property Position: Integer read FPosition write SetPosition;
  public                   
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    property Files: TStrings read FFiles write SetFiles;
    property Gradient: Boolean read FGradient write SetGradient default True;
    property OnImageClick: TNotifyEvent read FOnImageClick write SetOnImageClick;
  end;

  TcxThumbnailStrip = class(TcxCustomThumbnailStrip)
  published
    { Published declarations }
    property Align;     
    property Anchors;
    property Constraints;
    property Files;
    property Gradient;
    property Images;
    property ItemIndex;
    property LookAndFeel;   
    property OnCanResize;
    property OnConstrainedResize;
    property OnImageClick;
    property OnResize;
    property PaintStyle;
    property ShowBorder;
  end;

procedure Register;

implementation

uses Graphics, Messages, Windows, Forms; 

{ TcxCustomThumbnailStrip }

procedure Register;
begin
  RegisterComponents('cx-tras', [TcxThumbnailStrip]);
end;

constructor TcxCustomThumbnailStrip.Create(AOwner: TComponent);
var
  AFiles: TStringList;
begin
  inherited;
  Kind := gkHGradient;

  FItemIndex := -1;
  FRecreate := False;
  FLockCount := 0;
  FForwardButtonDown := False;
  FBackButtonDown := False;
  FPosition := 0;
  FImageLength := 0;
  FGradient := True;
  FOrientation := soUnknown;

  FOldHeight := Height;
  FOldWidth := Width;

  FList := TObjectList.Create;

  AFiles := TStringList.Create;
  AFiles.OnChange := HandleFilesChange;
  FFiles := AFiles;

  FBackButton := TcxOfficeGizmo.Create(Self);
  BackButton.Kind := gkButton;
  BackButton.Transparent := True;
  BackButton.ShowBorder := False;
  BackButton.Images := Images;
  BackButton.Alignment := taCenter;
  BackButton.ImageOffset := 0;
  BackButton.Parent := Self;
  BackButton.OnMouseDown := HandleBackButtonDown;
  BackButton.OnMouseUp := HandleBackButtonUp;

  FForwardButton := TcxOfficeGizmo.Create(Self);
  ForwardButton.Kind := gkButton;
  ForwardButton.Transparent := True;
  ForwardButton.ShowBorder := False;
  ForwardButton.Images := Images;
  ForwardButton.Alignment := taCenter;
  ForwardButton.ImageOffset := 0;
  ForwardButton.Parent := Self;
  ForwardButton.OnMouseDown := HandleForwardButtonDown;
  ForwardButton.OnMouseUp := HandleForwardButtonUp;

  FHolderPanel := TcxOfficeGizmo.Create(Self);
  HolderPanel.Align := alClient;
  HolderPanel.Parent := Self;
  HolderPanel.ShowBorder := False;
  HolderPanel.Kind := gkHGradient;
  HolderPanel.Transparent := True;
  HolderPanel.DoubleBuffered := True;
end;

destructor TcxCustomThumbnailStrip.Destroy;
begin                    
  FreeAndNil(FFiles);
  FreeAndNil(FList);
  FreeAndNil(FHolderPanel);
  FreeAndNil(FForwardButton);
  FreeAndNil(FBackButton);
  inherited;
end;

procedure TcxCustomThumbnailStrip.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TcxCustomThumbnailStrip.DoImageClick(Sender: TObject);
var
  Index: Integer;
  Holder: TPanel;
  I: Integer;
begin
  Holder := TPanel(TPanel(TImage(Sender).Parent).Parent);
  Index := -1;
  for I := 0 to List.Count - 1 do
    if List[I] = Holder then
    begin
      Index := I;
      Break;
    end;
  ItemIndex := Index;
  if Assigned(FOnImageClick) then
    FOnImageClick(Sender);
end;  

procedure TcxCustomThumbnailStrip.DoImageListChange;
begin
  inherited;
  BackButton.Images := Images;
  ForwardButton.Images := Images;
end;

procedure TcxCustomThumbnailStrip.EndUpdate;
begin
  if FLockCount > 0 then
    Dec(FLockCount);
  if (FLockCount = 0) and FRecreate then
    RecreateImages;
end;

procedure TcxCustomThumbnailStrip.HandleBackButtonDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FBackButtonDown := True;
  while FBackButtonDown do
  begin
    Position := Position - 10;
    UpdateButtons;
    Application.ProcessMessages;
    Sleep(10);
  end;
end;

procedure TcxCustomThumbnailStrip.HandleBackButtonUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FBackButtonDown := False;
  UpdateButtons;
end;

procedure TcxCustomThumbnailStrip.HandleFilesChange(Sender: TObject);
begin
  RecreateImages;
end;

procedure TcxCustomThumbnailStrip.HandleForwardButtonDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FForwardButtonDown := True;
  while FForwardButtonDown do
  begin
    Position := Position + 10;
    UpdateButtons;
    Application.ProcessMessages;
    Sleep(10);
  end;
end;

procedure TcxCustomThumbnailStrip.HandleForwardButtonUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FForwardButtonDown := False;
  UpdateButtons;
end;

procedure TcxCustomThumbnailStrip.HandleImageClick(Sender: TObject);
begin
  DoImageClick(Sender);
end;

procedure TcxCustomThumbnailStrip.RecreateImages;
var
  I, X, AWidth, AHeight, ATop, ALeft: Integer;
  Image: TImage;
  InnerPanel: TPanel;
  ImageHolder: TPanel;
  Ratio: Double;
  OldLength: Integer;
begin
  if csLoading in ComponentState then
    Exit;

  if FLockCount > 0 then
  begin 
    FRecreate := True;
    Exit;
  end;

  BeginUpdate;
  try

    List.Clear;
    OldLength := ImageLength;
    ImageLength := 0;
    X := 8;
    I := 0;
    while I < Files.Count do
    begin
      if FileExists(Files[I]) then
      begin

        ImageHolder := TPanel.Create(Self);
      
        Image := TImage.Create(ImageHolder);
        Image.AutoSize := True;
        try
          Image.Picture.LoadFromFile(Files[I]);
        except
          Files.Delete(I);
          FreeAndNil(ImageHolder);
          Continue;
        end;
        Image.AutoSize := False;
        Image.Stretch := True;

        Ratio := Image.Width / Image.Height;

        if Orientation = soVertical then
        begin
          AWidth := Width - 16;
          AHeight := Trunc(AWidth / Ratio);
          ALeft := 8;
          ATop := -Position + X;
          X := X + AHeight + 8;
        end else
        begin
          AHeight := Height - 16;
          AWidth := Trunc(AHeight * Ratio);
          ATop := 8;
          ALeft := -Position + X;
          X := X + AWidth + 8;
        end;

        if Assigned(FOnImageClick) then
          Image.Cursor := crHandPoint;

        Image.OnClick := HandleImageClick;  
        Image.Align := alClient;

        ImageHolder.BevelOuter := bvNone;
        ImageHolder.Caption := '';
        if I = ItemIndex then
        begin
          ImageHolder.Color := clHighlight;
        end else
        begin
          ImageHolder.Color := clBtnShadow;
        end;
        ImageHolder.BorderWidth := 1;

        ImageHolder.Height := AHeight;
        ImageHolder.Width := AWidth;
        ImageHolder.Top := ATop;
        ImageHolder.Left := ALeft;
        ImageHolder.ParentBackground := False;
        ImageHolder.FullRepaint := False;

        InnerPanel := TPanel.Create(ImageHolder);
        InnerPanel.BevelOuter := bvNone;
        InnerPanel.Caption := '';
        InnerPanel.Color := clBtnFace;
        InnerPanel.Align := alClient;
        InnerPanel.ParentBackground := False;
        InnerPanel.FullRepaint := False;

        Image.Parent := InnerPanel;
        InnerPanel.Parent := ImageHolder;
        ImageHolder.Parent := HolderPanel;

        Inc(I);
        List.Add(ImageHolder);

      end;
    end;
    if List.Count > 0 then
      ImageLength := X;

    if ImageLength < OldLength then
      Position := Position - (OldLength - ImageLength);

    UpdateButtons;

    FRecreate := False;
  finally
    EndUpdate;
  end;
  
  Invalidate;
end;

procedure TcxCustomThumbnailStrip.Resize;
begin
  inherited;

  BeginUpdate;
  try
    if Width >= Height then
      Orientation := soHorizontal
    else
      Orientation := soVertical;

    if FOldHeight <> Height then
    begin
      if Orientation = soHorizontal then
        RecreateImages
      else if (FOldHeight < Height) and not ForwardButton.Enabled then
        Position := Position - (Height - FOldHeight);
      FOldHeight := Height;
    end;
    if FOldWidth <> Width then
    begin
      if Orientation = soVertical then
        RecreateImages
      else if (FOldWidth < Width) and not ForwardButton.Enabled then
        Position := Position - (Width - FOldWidth);
      FOldWidth := Width;
    end;
      
    Realign;
  finally
    EndUpdate;
  end;
  UpdateButtons; 
end;

procedure TcxCustomThumbnailStrip.SetFiles(const Value: TStrings);
begin
  FFiles.Assign(Value);
end;

procedure TcxCustomThumbnailStrip.SetGradient(const Value: Boolean);
begin
  if FGradient <> Value then
  begin
    if not Value then
      Kind := gkPanel
    else begin
      if Orientation = soVertical then
        Kind := gkVGradient
      else
        Kind := gkHGradient;
    end;
    FGradient := Value;
  end;
end;

procedure TcxCustomThumbnailStrip.SetImageLength(const Value: Integer);
begin
  FImageLength := Value;
end;

procedure TcxCustomThumbnailStrip.SetItemIndex(const Value: Integer);
var
  I: Integer;
begin
  if not (csLoading in ComponentState) then
  begin
    for I := 0 to List.Count - 1 do
    begin
      TPanel(List[I]).Color := clBtnShadow;
    end;
    if Value >= 0 then
    begin
      TPanel(List[Value]).Color := clHighlight;
    end;
  end;
  FItemIndex := Value;
end;

procedure TcxCustomThumbnailStrip.SetOnImageClick(const Value: TNotifyEvent);
var
  I: Integer;
  Image: TImage;
begin
  FOnImageClick := Value;
  for I := 0 to List.Count - 1 do
  begin
    if List[I] is TImage then
    begin
      Image := TImage(List[I]);
      if Assigned(Value) then
      begin
        Image.Cursor := crHandPoint;
      end else
      begin
        Image.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TcxCustomThumbnailStrip.SetOrientation(
  const Value: TcxStripOrientation);
begin
  if FOrientation <> Value then
  begin
    if Value = soVertical then
    begin
      BackButton.Align := alTop;
      BackButton.Height := 22;
      BackButton.ImageIndex := 2;
      ForwardButton.Align := alBottom;
      ForwardButton.Height := 22;
      ForwardButton.ImageIndex := 3;
      if Gradient then
        Kind := gkVGradient;
    end else
    begin
      BackButton.Align := alLeft;
      BackButton.Width := 22;
      BackButton.ImageIndex := 0;
      ForwardButton.Align := alRight;
      ForwardButton.Width := 22;   
      ForwardButton.ImageIndex := 1;   
      if Gradient then
        Kind := gkHGradient;
    end;           
    RecreateImages;
    FOrientation := Value;
  end;
end;

procedure TcxCustomThumbnailStrip.SetPosition(const Value: Integer); 
var
  AValue: Integer;
  I, X: Integer;
  Holder: TPanel;
begin
  AValue := Value;

  if Orientation = soVertical then
  begin
    if AValue > ImageLength - HolderPanel.Height  then
      AValue := ImageLength - HolderPanel.Height;
  end else
  begin
    if AValue > ImageLength - HolderPanel.Width  then
      AValue := ImageLength - HolderPanel.Width;
  end;  

  if AValue < 0 then
    AValue := 0;

  if FPosition <> AValue then
  begin
    X := -AValue + 8;

    for I := 0 to List.Count - 1 do
    begin
      if List[I] is TPanel then
      begin
        Holder := TPanel(List[I]);

        if Orientation = soVertical then
        begin
          Holder.Top := X;
          X := X + Holder.Height + 8;
        end else
        begin
          Holder.Left := X;
          X := X + Holder.Width + 8;
        end;

      end;
    end;

    FPosition := AValue;
  end;
end;  

procedure TcxCustomThumbnailStrip.UpdateButtons;
begin
  BackButton.Enabled := Position > 0;
  if Orientation = soVertical then
    ForwardButton.Enabled := ImageLength > HolderPanel.Height + Position
  else
    ForwardButton.Enabled := ImageLength > HolderPanel.Width + Position;
  if not BackButton.Enabled then
    FBackButtonDown := False;
  if not ForwardButton.Enabled then
    FForwardButtonDown := False;
end;

end.
