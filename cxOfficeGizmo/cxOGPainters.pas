// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxOGPainters;

interface

uses cxOG, Windows, Graphics;

var
  dxOfficeHeaderTextColor: TColor = clWindow;
  dxOGForceStandardColors: Boolean = False;

type

  TcxOGCustomStandardPainter = class(TcxOGCustomPainter)
  protected
    function GetBorderColor: TColor; virtual;
    function GetFillColor1: TColor; virtual;
    function GetFillColor2: TColor; virtual;
    function GetTextColor: TColor; virtual;
    procedure PaintBackground(var ARect: TRect); virtual;
    procedure PaintContent(var ARect: TRect); virtual;
    procedure PaintFrame(var ARect: TRect); virtual;
  public
    procedure Paint; override;  
    property BorderColor: TColor read GetBorderColor;
    property FillColor1: TColor read GetFillColor1;
    property FillColor2: TColor read GetFillColor2;
    property TextColor: TColor read GetTextColor;
  end;

  TcxOGStandardPainter = class(TcxOGCustomStandardPainter)
  private                     
    FBitmap: TBitmap;
    procedure DrawStandardThemedBackground(var ARect: TRect);
    function IsNativePainting: Boolean;
    procedure DrawInvertedThemedBackground(var ARect: TRect);
  protected
    procedure PaintBackground(var ARect: TRect); override;
    procedure PaintContent(var ARect: TRect); override;
    procedure PaintFrame(var ARect: TRect); override;
    function GetTextColor: TColor; override;
  public
    destructor Destroy; override;  
  end;

  TcxOGOffice10Painter = class(TcxOGCustomStandardPainter)
  protected
    function GetBorderColor: TColor; override;
    function GetFillColor1: TColor; override;      
    function GetFillColor2: TColor; override;
    function GetTextColor: TColor; override;
  end;

  TcxOGOffice11Painter = class(TcxOGCustomStandardPainter)
  protected
    function GetBorderColor: TColor; override;
    function GetFillColor1: TColor; override;
    function GetFillColor2: TColor; override; 
    function GetTextColor: TColor; override;
    procedure PaintBackground(var ARect: TRect); override;
    procedure PaintContent(var ARect: TRect); override;
  end;

  TcxOGSkinPainter = class(TcxOGCustomStandardPainter)
  private
    FBitmap: TBitmap;
    procedure PaintSkinnedButton(var ARect: TRect);
    procedure PaintSkinnedHeader(var ARect: TRect);
    procedure PaintSkinnedHGradient(var ARect: TRect);
    procedure PaintSkinnedPanel(var ARect: TRect);
    procedure PaintSkinnedSplitter(var ARect: TRect);
    procedure PaintSkinnedVGradient(var ARect: TRect);
  protected
    function GetBorderColor: TColor; override;  
    function GetTextColor: TColor; override;
    procedure PaintBackground(var ARect: TRect); override;
    procedure PaintContent(var ARect: TRect); override;
    procedure PaintFrame(var ARect: TRect); override;
  public
    destructor Destroy; override;
  end;

implementation

uses dxOffice11, dxUxTheme, dxThemeConsts, dxThemeManager, Types, Classes,
  Controls, cxGraphics, cxLookAndFeelPainters, cxClasses, SysUtils, cxGeometry;

const
  dxGripperUnits = 9;
  dxGripperColor1 = $00FBF9F9;
  dxGripperColor2 = $00987461;
  dxGripperColor3 = $00463228;       

{ TcxOGCustomStandardPainter }

function TcxOGCustomStandardPainter.GetBorderColor: TColor;
begin
  Result := clBtnShadow;
end;

function TcxOGCustomStandardPainter.GetFillColor1: TColor;
begin
  case ParentInfo.Kind of
    gkHeader:
    begin
      if ParentInfo.PaintFocused then
        Result := clHighlight
      else
        Result := clBtnShadow;
    end;
    gkSplitter: Result := clBtnFace;
    gkButton, gk3DButton:
    begin
      if ParentInfo.Down and (ParentInfo.State = gsNormal) then
        Result := GetMiddleRGB(clBtnFace, clBtnHighlight, 50)
      else
        Result := clBtnFace;
    end;
  else
    if ParentInfo.PaintFocused then
      Result := clInactiveCaptionText
    else
      Result := clBtnFace;
  end;
end;   

function TcxOGCustomStandardPainter.GetFillColor2: TColor;
begin
  Result := GetFillColor1;
end;

function TcxOGCustomStandardPainter.GetTextColor: TColor;
begin
  case ParentInfo.Kind of
    gkHeader: Result := ParentControl.LookAndFeel.Painter.DefaultHeaderTextColor;
    gkButton, gk3DButton:
    begin
      Result := ParentControl.LookAndFeel.Painter.DefaultEditorTextColor(not ParentInfo.Enabled);
    end;
  else
    Result := ParentControl.LookAndFeel.Painter.DefaultEditorTextColor(False);
  end;
end;

procedure TcxOGCustomStandardPainter.Paint;
var
  ARect: TRect;
begin
  inherited;
  ARect := ParentInfo.ClientRect;
  if ParentInfo.PaintColor then
  begin
    ParentInfo.Canvas.Brush.Color := ParentInfo.Color;
    ParentInfo.Canvas.FillRect(ARect);
  end else if not (ParentInfo.Transparent and not (csDesigning in ParentControl.ComponentState)) or
    (((ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton)) and ((ParentInfo.State <> gsNormal) or
    ParentInfo.Down)) then
    PaintBackground(ARect);

  PaintFrame(ARect);
  PaintContent(ARect);
end;     

type
  TControlAccess = class(TControl);

procedure TcxOGCustomStandardPainter.PaintBackground(var ARect: TRect);
begin
  if ParentInfo.Transparent and (csDesigning in ParentControl.ComponentState)
    and Assigned(ParentControl.Parent) then
  begin
    ParentInfo.Canvas.Brush.Color := TControlAccess(ParentControl.Parent).Color;
    ParentInfo.Canvas.FillRect(ARect);
  end else
  begin
    if FillColor1 = FillColor2 then
    begin
      ParentInfo.Canvas.Brush.Color := FillColor1;
      ParentInfo.Canvas.FillRect(ARect);
    end else
    begin
      FillGradientRect(ParentInfo.Canvas.Handle, ARect, FillColor1, FillColor2,
        ParentInfo.Kind = gkHGradient);
    end;
  end;
end; 

procedure GetMenuFont(const AFont: TFont);
var
  NonClientMetrics: TNonClientMetrics;
begin
  NonClientMetrics.cbSize := SizeOf(NonClientMetrics);
  if SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, @NonClientMetrics, 0) then
    AFont.Handle := Windows.CreateFontIndirect(NonClientMetrics.lfMenuFont)
  else
    AFont.Handle := GetStockObject(DEFAULT_GUI_FONT);
end;     

procedure TcxOGCustomStandardPainter.PaintContent(var ARect: TRect);
var
  AHeight, AWidth: Integer;
  HasImage, HasCaption: Boolean;
  ImageHeight, CaptionHeight: Integer;
  CaptionY, ImageY: Integer;
  LeftOffset, RightOffset: Integer;
  ImageWidth, CaptionWidth: Integer;

  procedure AssignFont;
  begin
    with ParentInfo.Canvas do
    begin
      case ParentInfo.Kind of
        gkHeader:
        begin
          GetMenuFont(Font);
          Font.Name := 'Arial';
          Font.Size := Font.Size + (Font.Size div 2);
          if Font.Size < 12 then
            Font.Size := 12;
          Font.Style := [fsBold];
        end;
      else
        Font.Assign(ParentControl.Font);
      end;
      Font.Color := TextColor;
      Brush.Style := bsClear;
    end;
  end;

  procedure PaintLeftAlignedContent;  
  var
    X1, X2: Integer;
  begin
    X1 := LeftOffset;
    with ParentInfo do
    begin
      if HasImage then
      begin
        if ImageAlignment = iaLeft then
        begin
          X2 := X1 + ImageOffset;
          X1 := X2 + ImageWidth;
        end else
          X2 := AWidth - RightOffset - ImageOffset - ImageWidth;
        Images.Draw(Canvas.Canvas, X2 + ARect.Left, ImageY + ARect.Top,
          ImageIndex, Enabled);
      end;
      if HasCaption then
      begin
        X1 := X1 + CaptionOffset;
        Canvas.Canvas.TextOut(X1 + ARect.Left, CaptionY + ARect.Top, Caption);
      end;
    end;
  end;

  procedure PaintRightAlignedContent;
  var
    X1, X2: Integer;
  begin                 
    with ParentInfo do
    begin
      X1 := AWidth - RightOffset;
      if HasImage then
      begin
        if ImageAlignment = iaLeft then
          X2 := LeftOffset + ImageOffset
        else
        begin
          X2 := X1 - ImageOffset - ImageWidth;
          X1 := X2;
        end;

        Images.Draw(Canvas.Canvas, X2 + ARect.Left, ImageY + ARect.Top,
          ImageIndex, Enabled);
      end;
      if HasCaption then
      begin
        X1 := X1 - CaptionOffset - CaptionWidth;
        Canvas.Canvas.TextOut(X1 + ARect.Left, CaptionY + ARect.Top, Caption);
      end;
    end;
  end;     

  procedure PaintCenterAlignedContent;
  var
    X1, X2, ContentWidth: Integer;
  begin
    ContentWidth := ImageWidth;
    with ParentInfo do
    begin
      if (ContentWidth > 0) and HasCaption then
      begin
        if ImageAlignment = iaLeft then
          ContentWidth := ContentWidth + CaptionOffset
        else
          ContentWidth := ContentWidth + ImageOffset;
      end;
      ContentWidth := ContentWidth + CaptionWidth;
      X1 := (AWidth div 2) - (ContentWidth div 2);
      if HasImage then
      begin
        if ImageAlignment = iaLeft then
        begin
          X2 := X1;
          X1 := X2 + ImageWidth + CaptionOffset;
        end else
        begin
          X2 := X1 + ContentWidth - ImageWidth;
        end;
        Images.Draw(Canvas.Canvas, X2 + ARect.Left, ImageY + ARect.Top,
          ImageIndex, Enabled);
      end;
      if HasCaption then
      begin
        Canvas.Canvas.TextOut(X1 + ARect.Left, CaptionY + ARect.Top, Caption);
      end;
    end;
  end;

  procedure PaintHorizontalContent;
  begin
    case ParentInfo.Alignment of
      taRightJustify: PaintRightAlignedContent;
      taCenter: PaintCenterAlignedContent;
    else
      PaintLeftAlignedContent;
    end;
  end;  

  procedure PaintVerticalContent;
  var
    ContentHeight: Integer;
    Y1, Y2: Integer;
  begin
    ContentHeight := ImageHeight;
    with ParentInfo do
    begin
      if (ContentHeight > 0) and HasCaption then
      begin
        if ImageAlignment = iaTop then
          ContentHeight := ContentHeight + CaptionOffset
        else
          ContentHeight := ContentHeight + ImageOffset;
      end;
      if HasCaption then
        ContentHeight := ContentHeight + CaptionHeight;

      Y1 := (AHeight div 2) - (ContentHeight div 2);

      if HasImage then
      begin
        if ImageAlignment = iaTop then
        begin
          Y2 := Y1;
          Y1 := Y2 + ImageHeight + CaptionOffset;
        end else
        begin
          Y2 := Y1 + ContentHeight - ImageHeight;
        end;
        Images.Draw(Canvas.Canvas, (AWidth div 2) - (ImageWidth div 2),
          Y2 + ARect.Top, ImageIndex, Enabled);
      end;

      if HasCaption then
        Canvas.Canvas.TextOut((AWidth div 2) - (CaptionWidth div 2), Y1 + ARect.Top, Caption);

    end;
  end;

  procedure PaintSplitterSign;
  var
    I, H, V: Integer;
  begin
    with ParentInfo do
    begin
      if Width > Height then
      begin
        V := ARect.Top + (ARect.Bottom - ARect.Top) div 2;
        H := ARect.Left + ((ARect.Right - ARect.Left) div 2) -
          (((dxGripperUnits * 4) - 2) div 2);
        for I := 0 to dxGripperUnits - 1 do
        begin
          Canvas.Pixels[H, V - 1] := dxGripperColor3;
          Canvas.Pixels[H + 1, V - 1] := dxGripperColor3;
          Canvas.Pixels[H, V] := dxGripperColor3;
          Canvas.Pixels[H + 1, V] := dxGripperColor2;
          Canvas.Pixels[H + 2, V] := dxGripperColor1;
          Canvas.Pixels[H + 1, V + 1] := dxGripperColor1;
          Canvas.Pixels[H + 2, V + 1] := dxGripperColor1;
          H := H + 4;
        end;
      end
      else
      begin                                    
        V := ARect.Top + (ARect.Bottom - ARect.Top) div 2 -
          (((dxGripperUnits * 4) - 2) div 2);
        H := ARect.Left + ((ARect.Right - ARect.Left) div 2);
        for I := 0 to dxGripperUnits - 1 do
        begin
          Canvas.Pixels[H, V - 1] := dxGripperColor3;
          Canvas.Pixels[H + 1, V - 1] := dxGripperColor3;
          Canvas.Pixels[H, V] := dxGripperColor3;
          Canvas.Pixels[H + 1, V] := dxGripperColor2;
          Canvas.Pixels[H + 2, V] := dxGripperColor1;
          Canvas.Pixels[H + 1, V + 1] := dxGripperColor1;
          Canvas.Pixels[H + 2, V + 1] := dxGripperColor1;
          V := V + 4;
        end;
      end;
    end;
  end;

begin
  LeftOffset := 0;
  RightOffset := 0;

  AHeight := ARect.Bottom - ARect.Top;
  AWidth := ARect.Right - ARect.Left;

  with ParentInfo do
  begin

    if ShowBorder and (bLeft in Borders) then
      LeftOffset := BorderWidth;

    if (mLeft in Margins) then
      Inc(LeftOffset, Margin);

    if ShowBorder and (bRight in Borders) then
      RightOffset := BorderWidth;

    if (mRight in Margins) then
      Inc(RightOffset, Margin);

    HasCaption :=  ShowCaption and (Caption <> '');

    CaptionWidth := 0;
    CaptionY := AHeight div 2;  

    HasImage := Assigned(Images) and ((ImageIndex >= 0) and
      (ImageIndex < Images.Count));

    ImageWidth := 0;
    ImageHeight := 0;
    ImageY := CaptionY;

    if HasCaption then
    begin
      AssignFont;
      CaptionWidth := Canvas.TextWidth(Caption);
      CaptionHeight := Canvas.TextHeight(Caption);
      CaptionY := (AHeight div 2) - (CaptionHeight div 2);
    end;

    if ParentInfo.Kind = gkSplitter then
      PaintSplitterSign
    else
    begin  
      if HasImage then
      begin
        ImageWidth := Images.Width;
        ImageHeight := Images.Height;
        ImageY := (AHeight div 2) - (ImageHeight div 2);
      end;

      if HasCaption or HasImage then
      begin
        if ParentInfo.ImageAlignment in [iaTop, iaBottom] then
        begin
          PaintVerticalContent;
        end else
        begin
          PaintHorizontalContent;
        end;
      end;
    end;
  end; 
end;

procedure TcxOGCustomStandardPainter.PaintFrame(var ARect: TRect);
begin
  if (ParentInfo.ShowBorder) and (ParentInfo.BorderWidth > 0) and
    (ParentInfo.Borders <> []) and

    (ParentInfo.Down or not (not (csDesigning in ParentControl.ComponentState) and
    (ParentInfo.Transparent and not (csDesigning in ParentControl.ComponentState)) and   
    (ParentInfo.Kind = gkButton) and
    (ParentInfo.State = gsNormal))) then
  begin
    ParentInfo.Canvas.FrameRect(ARect, BorderColor, ParentInfo.BorderWidth, ParentInfo.Borders);
  end;
end;

{ TcxOGStandardPainter }

function TcxOGStandardPainter.IsNativePainting: Boolean;
begin
  if csDestroying in ParentControl.ComponentState then
    Result := False
  else
    Result := AreVisualStylesAvailable and (OpenTheme(totRebar) <> TC_NONE);
end;     

destructor TcxOGStandardPainter.Destroy;
begin
  FreeAndNil(FBitmap);
  inherited;
end;

procedure TcxOGStandardPainter.DrawStandardThemedBackground(var ARect: TRect);
var
  Theme: TdxTheme;
begin
  Theme := OpenTheme(totRebar);
  if Theme > 0  then
  begin
    DrawThemeBackground(Theme, ParentInfo.Canvas.Handle, 0, 0, Rect(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom + 3), nil);
  end;
end;

function TcxOGStandardPainter.GetTextColor: TColor;
begin
  case ParentInfo.Kind of
    gkHeader: Result := clBtnText;
    gkButton, gk3DButton:
    begin
      Result := clBtnText;
    end;
  else
    Result := clWindowText;
  end;
end;

procedure TcxOGStandardPainter.DrawInvertedThemedBackground(var ARect: TRect);
var
  NewRect: TRect;
  Canvas: TcxCanvas;  
  Theme: TdxTheme;
begin          
  Theme := OpenTheme(totRebar);
  if Theme > 0  then
  begin
    if not Assigned(FBitmap) then
      FBitmap := TBitmap.Create;

    FBitmap.Height := ParentInfo.Height;
    FBitmap.Width := ParentInfo.Width;
    NewRect := Rect(0, 0, FBitmap.Width, FBitmap.Height);
    InflateRect(NewRect, 2, 2);
    try
      Canvas := TcxCanvas.Create(FBitmap.Canvas);

      DrawThemeBackground(Theme, Canvas.Handle, 0, 0, Rect(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom + 3), nil);

      Canvas.RotateBitmap(FBitmap, ra180);

      ParentInfo.Canvas.Draw(0, 0, FBitmap);
    finally
      FreeAndNil(Canvas);
    end;
  end;
end;

procedure TcxOGStandardPainter.PaintBackground(var ARect: TRect);
var
  Theme: TdxTheme;
  AState: Integer;
begin
  if (ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton) then
  begin
    FillRectByColor(ParentInfo.Canvas.Handle, ARect, FillColor1);
    if IsNativePainting then
    begin      
      Theme := OpenTheme(totToolBar);
      if Theme > 0 then
      begin
        if not ParentInfo.Enabled then
          AState := TS_DISABLED
        else
        begin
          if ParentInfo.State = gsPressed then
            AState := TS_PRESSED
          else if ParentInfo.Down then
          begin
            if ParentInfo.State = gsHot then
              AState := TS_HOTCHECKED
            else
              AState := TS_CHECKED;
          end else if (ParentInfo.State = gsHot) or (ParentInfo.Kind = gk3DButton) then
            AState := TS_HOT
          else
            AState := TS_NORMAL;
        end;
        DrawThemeBackground(Theme, ParentInfo.Canvas.Handle, TP_BUTTON,
          AState, ARect, nil);
      end;
    end;
  end else if IsNativePainting and
    (ParentInfo.Kind in [gkSplitter, gkHGradient, gkVGradient]) then
  begin
    if ParentInfo.PaintFocused or ParentInfo.Down then
      DrawInvertedThemedBackground(ARect)
    else
      DrawStandardThemedBackground(ARect);
  end else
    inherited;
end;

procedure TcxOGStandardPainter.PaintContent(var ARect: TRect); 
  procedure PaintXPSplitterSign;
  var
    ATheme: TdxTheme;
    GripRect: TRect;
    APartId: Integer;
  begin
    with ParentInfo do
    begin
      ATheme := OpenTheme(totRebar);
      if Width > Height then
      begin
        APartId := RP_GRIPPERVERT;
        GripRect.Top := (ARect.Top + ((ARect.Bottom - ARect.Top) div 2)) - 3;
        GripRect.Bottom := GripRect.Top + 6;
        GripRect.Left := ((ARect.Right - ARect.Left) -
          (dxGripperUnits * 5)) div 2;
        GripRect.Right := GripRect.Left + (dxGripperUnits * 5) - 2;
      end
      else
      begin
        APartId := RP_GRIPPER;
        GripRect.Top := ((ARect.Bottom - ARect.Top) -
          (dxGripperUnits * 5)) div 2;
        GripRect.Bottom := GripRect.Top + (dxGripperunits * 5) - 3;
        GripRect.Left := (ARect.Left + ((ARect.Right - ARect.Left) div 2)) - 3;
        GripRect.Right := GripRect.Left + 6;
      end;
      DrawThemeBackground(ATheme, Canvas.Handle, APartId, CHEVS_NORMAL,
        @GripRect);
    end;
  end;
begin
  if ((ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton)) and
    ((ParentInfo.State = gsPressed) or ParentInfo.Down) and
    not IsNativePainting then
    OffsetRect(ARect, 1, 1);
  if (ParentInfo.Kind = gkSplitter) and IsNativePainting then
    PaintXPSplitterSign
  else
    inherited;
end;

procedure TcxOGStandardPainter.PaintFrame(var ARect: TRect);
begin
  if (ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton) then
  begin
    if not IsNativePainting then
    begin
      if (ParentInfo.State = gsPressed) or ParentInfo.Down then
        DrawEdge(ParentInfo.Canvas.Handle, ARect, BDR_SUNKENOUTER, BF_RECT)
      else if (ParentInfo.State = gsHot) or (ParentInfo.Kind = gk3DButton) then
        DrawEdge(ParentInfo.Canvas.Handle, ARect, BDR_RAISEDINNER, BF_RECT);
    end;
  end else
    inherited;
end;           

{ TcxOGOffice11Painter }

function TcxOGOffice11Painter.GetBorderColor: TColor;
begin
  if (ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton) then
  begin
    if not ParentInfo.Enabled then
      Result := dxOffice11TextDisabledColor
    else if ParentInfo.Down then
      Result := dxOffice11SelectedBorderColor
    else
      case ParentInfo.State of
        gsHot, gsPressed: Result := dxOffice11SelectedBorderColor;
      else
        if ParentInfo.Kind = gk3DButton then
          Result := dxOffice11SelectedBorderColor
        else
          Result := clBtnText
      end;
  end else
  begin
    if IsXPStandardScheme or dxOGForceStandardColors then
    begin
      if ParentInfo.PaintFocused then
        Result := dxOffice11NavPaneGroupCaptionPressedHotColor1
      else
        Result := dxOffice11NavPaneBorder;
    end else
      Result := clBtnShadow;
  end;
end;

function TcxOGOffice11Painter.GetFillColor1: TColor;
begin
  case ParentInfo.Kind of   
    gkButton, gk3DButton:
    begin
      if ParentInfo.PaintFocused then
      begin
        if ParentInfo.Down then
        begin
          case ParentInfo.State of
            gsHot: Result := dxOffice11DownedSelectedColor;
            gsPressed: Result := dxOffice11SelectedDownColor1;
          else
            Result := dxOffice11DownedColor;
          end;
        end else
        begin
          case ParentInfo.State of
            gsHot: Result := dxOffice11SelectedColor2;
            gsPressed: Result := dxOffice11SelectedDownColor2;
          else
            if ParentInfo.Kind = gk3DButton then
              Result := dxOffice11SelectedColor2
            else
              Result := dxOffice11SelectedColor1;
          end;
        end;
      end else
      begin
        if ParentInfo.Down then
        begin
          case ParentInfo.State of
            gsHot: Result := dxOffice11DownedSelectedColor;
            gsPressed: Result := dxOffice11SelectedDownColor1;
          else
            Result := dxOffice11DownedColor
          end;
        end else
        begin
          case ParentInfo.State of
            gsHot: Result := dxOffice11SelectedColor1;
            gsPressed: Result := dxOffice11SelectedDownColor1;
          else
            if ParentInfo.Kind = gk3DButton then
              Result := dxOffice11SelectedColor1
            else
              Result := dxOffice11DockColor1;
          end;
        end;
      end;
    end;
    gkPanel:
    begin
      if IsXPStandardScheme or dxOGForceStandardColors then
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11SelectedColor1
        else
          Result := dxOffice11DockColor2
      end else
        Result := GetMiddleRGB(dxOffice11DockColor1,
          dxOffice11DockColor2, 50);
    end;
    gkHeader:
    begin
      if IsXPStandardScheme or dxOGForceStandardColors then
      begin
        if ParentInfo.Down then
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionPressedColor2
          else
            Result := dxOffice11NavPaneHeaderColor2;
        end else
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionHotColor2
          else
            Result := dxOffice11NavPaneHeaderColor1;
        end;
      end else
      begin
        if ParentInfo.PaintFocused then
          Result := clHighlight
        else
          Result := clBtnShadow;
      end;
    end;
    gkSplitter:
    begin
      if IsXPStandardScheme or dxOGForceStandardColors then
      begin
        if ParentInfo.Down then
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionPressedColor2
          else
            Result := dxOffice11NavPaneSplitterColor2
        end else
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionHotColor2
          else
            Result := dxOffice11NavPaneSplitterColor1;
        end;
      end else
        Result := clBtnFace;
    end;
    gkHGradient:
    begin
      if ParentInfo.Down then
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionPressedColor1
        else
          Result := dxOffice11DockColor2;
      end else
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionHotColor2
        else
          Result := dxOffice11DockColor1;
      end;
    end;
    gkVGradient:
    begin
      if ParentInfo.Down then
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionHotColor2
        else
          Result := dxOffice11DockColor1;
      end else
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionPressedColor1
        else
          Result := dxOffice11DockColor2;
      end;
    end;
  else
    Result := dxOffice11DockColor1;
  end;
end;

function TcxOGOffice11Painter.GetFillColor2: TColor;
begin
  case ParentInfo.Kind of
    gkButton, gk3DButton: Result := GetFillColor1;
    gkPanel: Result :=  GetFillColor1;
    gkHeader:
    begin
      if IsXPStandardScheme or dxOGForceStandardColors then
      begin
        if ParentInfo.Down then
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionHotColor2
          else
            Result := dxOffice11NavPaneHeaderColor1;
        end else
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionPressedColor2
          else
            Result := dxOffice11NavPaneHeaderColor2;
        end;
      end else
        Result := GetFillColor1;
    end;
    gkSplitter:
    begin
      if IsXPStandardScheme or dxOGForceStandardColors then
      begin
        if ParentInfo.Down then
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionHotColor2
          else
            Result := dxOffice11NavPaneSplitterColor1;
        end else
        begin
          if ParentInfo.PaintFocused then
            Result := dxOffice11NavPaneGroupCaptionPressedColor2
          else
            Result := dxOffice11NavPaneSplitterColor2
        end;
      end else
        Result := clBtnShadow;
    end;
    gkHGradient:
    begin
      if ParentInfo.Down then
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionHotColor2
        else
          Result := dxOffice11DockColor1;
      end else
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionPressedColor1
        else
          Result := dxOffice11DockColor2;
      end;
    end;
    gkVGradient:
    begin
      if ParentInfo.Down then
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionPressedColor1
        else
          Result := dxOffice11DockColor2;
      end else
      begin
        if ParentInfo.PaintFocused then
          Result := dxOffice11NavPaneGroupCaptionHotColor2
        else
          Result := dxOffice11DockColor1;
      end;
    end;
  else
    Result := dxOffice11DockColor1;
  end;
end;

function TcxOGOffice11Painter.GetTextColor: TColor;
begin
  if (ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton) then
  begin
    if not ParentInfo.Enabled then
      Result := dxOffice11TextDisabledColor
    else if (ParentInfo.State = gsPressed) and not
      (IsXPStandardScheme or dxOGForceStandardColors) then
      Result := clHighlightText
    else
      Result := dxOffice11TextEnabledColor;
  end else if ParentInfo.Kind = gkHeader then
    Result := dxOffice11NavPaneHeaderFontColor
  else
    Result := inherited GetTextColor;
end;

procedure TcxOGOffice11Painter.PaintBackground(var ARect: TRect);
begin
  if (ParentInfo.Kind = gkSplitter) and (FillColor1 <> FillColor2) then
  begin
    FillGradientRect(ParentInfo.Canvas.Handle, ARect, FillColor1, FillColor2,
      ParentInfo.Height > ParentInfo.Width);
  end else
    inherited;
end;

procedure TcxOGOffice11Painter.PaintContent(var ARect: TRect);
begin
  if ParentInfo.Kind = gkSplitter then
    OffsetRect(ARect, -1, -1);

  inherited;   
end;

{ TcxOGOffice10Painter }

function TcxOGOffice10Painter.GetBorderColor: TColor;
begin
  if (ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton) then
  begin
    if not ParentInfo.Enabled then
      Result := clBtnShadow
    else if ParentInfo.Down then
      Result := clHighlight
    else
      case ParentInfo.State of
        gsHot, gsPressed: Result := clHighlight;
      else
        if ParentInfo.Kind = gk3DButton then
          Result := clHighlight
        else
          Result := clBtnText
      end;
  end else
    Result := inherited GetBorderColor;
end;

function TcxOGOffice10Painter.GetFillColor1: TColor;
begin
  if ((ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton)) and ParentInfo.Enabled then
  begin
    if ParentInfo.Down then
    begin
      case ParentInfo.State of
        gsHot: Result := GetLightDownedSelColor;
        gsPressed: Result := GetLightDownedSelColor;
      else
        Result := GetLightDownedColor;
      end;
    end else
    begin
      case ParentInfo.State of
        gsHot: Result := GetLightSelColor;
        gsPressed: Result := GetLightDownedSelColor;
      else
        if ParentInfo.Kind = gk3DButton then      
          Result := GetLightSelColor
        else
          Result := inherited GetFillColor1;
      end;
    end;
  end else
    Result := inherited GetFillColor1;
end;

function TcxOGOffice10Painter.GetFillColor2: TColor;
begin
  if (ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton) then
    Result := GetFillColor1
  else
    Result := inherited GetFillColor2;
end;

function TcxOGOffice10Painter.GetTextColor: TColor;
begin
  if ((ParentInfo.Kind = gkButton) or (ParentInfo.Kind = gk3DButton)) and (ParentInfo.State = gsPressed) then
    Result := clHighlightText
  else
    Result := inherited GetTextColor;
end;             

{ TcxOGSkinPainter }

destructor TcxOGSkinPainter.Destroy;
begin
  FreeAndNil(FBitmap);
  inherited;
end;

function TcxOGSkinPainter.GetBorderColor: TColor;
begin
  Result := ParentControl.LookAndFeel.Painter.GetContainerBorderColor(ParentInfo.PaintFocused);
end;

function TcxOGSkinPainter.GetTextColor: TColor;
begin
  Result := inherited GetTextColor;
end;

procedure TcxOGSkinPainter.PaintBackground(var ARect: TRect);
begin
  case ParentInfo.Kind of
    gkPanel: PaintSkinnedPanel(ARect);
    gkHeader: PaintSkinnedHeader(ARect);
    gkButton, gk3DButton: PaintSkinnedButton(ARect);
    gkHGradient: PaintSkinnedHGradient(ARect);
    gkVGradient: PaintSkinnedVGradient(ARect);
    gkSplitter: PaintSkinnedSplitter(ARect);
  end;
end;

procedure TcxOGSkinPainter.PaintContent(var ARect: TRect);
begin
  if ParentInfo.Kind <> gkSplitter then
    inherited;
end;

procedure TcxOGSkinPainter.PaintFrame(var ARect: TRect);
begin
  if (ParentInfo.Kind <> gkButton) and (ParentInfo.Kind <> gk3DButton) then
    inherited;
end;

procedure TcxOGSkinPainter.PaintSkinnedButton(var ARect: TRect);
var
  State: TcxButtonState;
begin
  State := cxbsDefault;
  case ParentInfo.State of
    gsNormal: State := cxbsNormal;
    gsHot: State := cxbsHot;
    gsPressed: State := cxbsPressed;
  end;
  if not ParentInfo.Enabled then
    State := cxbsDisabled
  else if ParentInfo.Down then
    State := cxbsPressed;
  ParentControl.LookAndFeel.Painter.DrawButton(ParentInfo.Canvas, ARect, '', State);
end;

procedure TcxOGSkinPainter.PaintSkinnedHeader(var ARect: TRect);
var
  ALargerRect: TRect;
begin
  ALargerRect := ARect;
  InflateRect(ALargerRect, 1, 1);
  if ParentInfo.PaintFocused then
    ParentControl.LookAndFeel.Painter.DrawGroupBoxCaption(ParentInfo.Canvas, ALargerRect, cxNullRect, cxgpBottom)
  else
    ParentControl.LookAndFeel.Painter.DrawGroupBoxCaption(ParentInfo.Canvas, ALargerRect, cxNullRect, cxgpTop);
end;    

procedure TcxOGSkinPainter.PaintSkinnedVGradient(var ARect: TRect);
var
  NewRect: TRect;
begin
  NewRect := ARect;
  InflateRect(NewRect, 2, 2);
  if ParentInfo.PaintFocused then
    ParentControl.LookAndFeel.Painter.DrawHeader(ParentInfo.Canvas, NewRect, NewRect, [], [], cxbsPressed,
      taLeftJustify, vaTop, False, False, '', ParentControl.Font, ParentControl.Font.Color, clNone)
  else
    ParentControl.LookAndFeel.Painter.DrawHeader(ParentInfo.Canvas, NewRect, NewRect, [], [], cxbsNormal,
      taLeftJustify, vaTop, False, False, '', ParentControl.Font, ParentControl.Font.Color, clNone);
end;

procedure TcxOGSkinPainter.PaintSkinnedHGradient(var ARect: TRect);
//var
//  NewRect: TRect;
//  Canvas: TcxCanvas;
begin
  PaintSkinnedVGradient(ARect);
//THIS IS WAY SLOW
//  if not Assigned(FBitmap) then
//    FBitmap := TBitmap.Create;
//
//  FBitmap.Height := ParentInfo.Width;
//  FBitmap.Width := ParentInfo.Height;
//  NewRect := Rect(0, 0, FBitmap.Width, FBitmap.Height);
//  InflateRect(NewRect, 2, 2);
//  try
//    Canvas := TcxCanvas.Create(FBitmap.Canvas);
//
//    if ParentInfo.PaintFocused then
//      ParentControl.LookAndFeel.Painter.DrawHeader(Canvas, NewRect, NewRect, [], [], cxbsPressed,
//        taLeftJustify, vaTop, False, False, '', ParentControl.Font, ParentControl.Font.Color, clNone)
//    else
//      ParentControl.LookAndFeel.Painter.DrawHeader(Canvas, NewRect, NewRect, [], [], cxbsNormal,
//        taLeftJustify, vaTop, False, False, '', ParentControl.Font, ParentControl.Font.Color, clNone);
//
//    Canvas.RotateBitmap(FBitmap, raPlus90);
//
//    ParentInfo.Canvas.Draw(0, 0, FBitmap);
//  finally
//    FreeAndNil(Canvas);
//  end;
end;

procedure TcxOGSkinPainter.PaintSkinnedPanel(var ARect: TRect);
var
  NewRect: TRect;
begin
  NewRect := ARect;
  InflateRect(NewRect, 2, 2);
  ParentInfo.Canvas.SaveClipRegion;
  ParentControl.LookAndFeel.Painter.DrawGroupBoxContent(ParentInfo.Canvas, NewRect, cxgpCenter);
  ParentInfo.Canvas.RestoreClipRegion;
end;

procedure TcxOGSkinPainter.PaintSkinnedSplitter(var ARect: TRect);
begin
  if (ParentControl.Align in [alTop, alBottom]) then
    ParentControl.Height := ParentControl.LookAndFeel.Painter.GetSplitterSize(ParentInfo.Width > ParentInfo.Height).cy
  else if (ParentControl.Align in [alLeft, alRight]) then
    ParentControl.Width := ParentControl.LookAndFeel.Painter.GetSplitterSize(ParentInfo.Width > ParentInfo.Height).cx;
  ParentControl.LookAndFeel.Painter.DrawSplitter(ParentInfo.Canvas, ARect, ParentInfo.PaintFocused,
    ParentInfo.Down, ParentInfo.Width > ParentInfo.Height);
end;

end.
