// This code is distributed under the terms and conditions of the MIT license.

// Copyright (c) 2012 Nathanial Woolls
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

unit cxOG;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, cxGraphics, Windows, cxLookAndFeels,
  ImgList, Messages, ActnList, Graphics;

type                        
  TcxPaintStyle = (psStandard, psOffice10, psOffice11, psUseLookAndFeel);

  TcxGizmoKind = (gkPanel, gkHeader, gkButton, gkHGradient, gkVGradient, gkSplitter, gk3DButton);

  TcxMargin = (mTop, mBottom, mLeft, mRight);
  TcxMargins = set of TcxMargin;

  TcxGizmoState = (gsNormal, gsHot, gsPressed);

  TcxImageAlignment = (iaLeft, iaRight, iaTop, iaBottom);

  TcxCustomOfficeGizmo = class;

  TcxOfficeGizmoActionLink = class(TControlActionLink)
  protected          
    FClient: TcxCustomOfficeGizmo;
    procedure AssignClient(AClient: TObject); override;
    function IsCheckedLinked: Boolean; override;
    function IsImageIndexLinked: Boolean; override;
    procedure SetChecked(Value: Boolean); override;
    procedure SetImageIndex(Value: Integer); override;
  end;
  
  TcxOGPainterParentInfo = class(TObject)
  private
    FCanvas: TcxCanvas;
    FParentControl: TcxCustomOfficeGizmo;
    function GetAlignment: TAlignment;
    function GetBorders: TcxBorders;
    function GetBorderWidth: Integer;
    function GetCanvas: TcxCanvas;
    function GetCaption: string;
    function GetCaptionOffset: Integer;
    function GetClientRect: TRect;
    function GetColor: TColor;
    function GetDown: Boolean;
    function GetEnabled: Boolean;
    function GetHandle: HWND;
    function GetHeight: Integer;
    function GetImageAlignment: TcxImageAlignment;
    function GetImageIndex: TImageIndex;
    function GetImageOffset: Integer;
    function GetImages: TCustomImageList;
    function GetKind: TcxGizmoKind;
    function GetMargin: Integer;
    function GetMargins: TcxMargins;
    function GetPaintColor: Boolean;
    function GetPaintFocused: Boolean;
    function GetShowBorder: Boolean;
    function GetShowCaption: Boolean;
    function GetState: TcxGizmoState;
    function GetTransparent: Boolean;
    function GetWidth: Integer;
  protected
    property ParentControl: TcxCustomOfficeGizmo read FParentControl;
  public
    constructor Create(AParent: TcxCustomOfficeGizmo);
    destructor Destroy; override;
    property Alignment: TAlignment read GetAlignment;
    property Borders: TcxBorders read GetBorders;
    property BorderWidth: Integer read GetBorderWidth;
    property Canvas: TcxCanvas read GetCanvas;
    property Caption: string read GetCaption;
    property CaptionOffset: Integer read GetCaptionOffset;
    property ClientRect: TRect read GetClientRect;
    property Color: TColor read GetColor;
    property Down: Boolean read GetDown;
    property Enabled: Boolean read GetEnabled;
    property Handle: HWND read GetHandle;
    property Height: Integer read GetHeight;
    property ImageAlignment: TcxImageAlignment read GetImageAlignment;
    property ImageIndex: TImageIndex read GetImageIndex;
    property ImageOffset: Integer read GetImageOffset;
    property Images: TCustomImageList read GetImages;
    property Kind: TcxGizmoKind read GetKind;
    property Margin: Integer read GetMargin;
    property Margins: TcxMargins read GetMargins;
    property PaintColor: Boolean read GetPaintColor;
    property PaintFocused: Boolean read GetPaintFocused;
    property ShowBorder: Boolean read GetShowBorder;
    property ShowCaption: Boolean read GetShowCaption;
    property State: TcxGizmoState read GetState;
    property Transparent: Boolean read GetTransparent;
    property Width: Integer read GetWidth;
  end;

  TcxOGCustomPainter = class(TObject)
  private
    FParentControl: TcxCustomOfficeGizmo;
    FParentInfo: TcxOGPainterParentInfo;
  protected
    property ParentControl: TcxCustomOfficeGizmo read FParentControl;
    property ParentInfo: TcxOGPainterParentInfo read FParentInfo;
  public
    constructor Create(AParent: TcxCustomOfficeGizmo);
    destructor Destroy; override;
    procedure Paint; virtual; abstract;
  end;

  TcxOGPainterClass = class of TcxOGCustomPainter;

  TcxCustomOfficeGizmo = class(TCustomPanel, IdxSkinSupport)
  private
    FAutoCheck: Boolean;
    FBorders: TcxBorders;
    FCaptionOffset: Integer;
    FDown: Boolean;
    FImageAlignment: TcxImageAlignment;
    FImageChangeLink: TChangeLink;
    FImageIndex: TImageIndex;
    FImageList: TCustomImageList;
    FImageOffset: Integer;
    FKind: TcxGizmoKind;
    FLookAndFeel: TcxLookAndFeel;
    FMargin: Integer;
    FMargins: TcxMargins;
    FMouseOver: Boolean;
    FOnMouseEnter: TNotifyEvent;
    FOnMouseLeave: TNotifyEvent;
    FPaintColor: Boolean;
    FPainter: TcxOGCustomPainter;
    FPaintFocused: Boolean;
    FPaintStyle: TcxPaintStyle;
    FParentStyle: Boolean;
    FShowBorder: Boolean;   
    FShowCaption: Boolean;
    FState: TcxGizmoState;
    FTransparent: Boolean;
    procedure CMEnabledchanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;  
    function ControlNeedsInvalidate(const AControl: TControl): Boolean;
    function GetActualPaintStyle: TcxPaintStyle;
    function GetPaintStyle: TcxPaintStyle;
    procedure InvalidateControl(const AControl: TControl; const Force: Boolean = False);
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues);
    procedure SetAutoCheck(const Value: Boolean);
    procedure SetBorders(const Value: TcxBorders);
    procedure SetCaptionOffset(const Value: Integer);
    procedure SetDown(const Value: Boolean);
    procedure SetImageAlignment(const Value: TcxImageAlignment);
    procedure SetImageIndex(const Value: TImageIndex);
    procedure SetImageList(const Value: TCustomImageList);
    procedure SetImageOffset(const Value: Integer);
    procedure SetKind(const Value: TcxGizmoKind);
    procedure SetLookAndFeel(const Value: TcxLookAndFeel);
    procedure SetMargin(const Value: Integer);
    procedure SetMargins(const Value: TcxMargins);
    procedure SetPaintColor(const Value: Boolean);
    procedure SetPaintFocused(const Value: Boolean);
    procedure SetPaintStyle(const Value: TcxPaintStyle);
    procedure SetParentStyle(const Value: Boolean);
    procedure SetShowBorder(const Value: Boolean);
    procedure SetShowCaption(const Value: Boolean); 
    procedure SetTransparent(const Value: Boolean);
    function GetCaptionWidth: Integer;
  protected
    procedure ActionChange(Sender: TObject; CheckDefaults: Boolean); override;
    procedure AdjustClientRect(var Rect: TRect); override;
    procedure DblClick; override;
    procedure DoImageListChange; virtual;
    function GetActionLinkClass: TControlActionLinkClass; override;
    function GetPainter: TcxOGCustomPainter; virtual;
    procedure ImageListChange(Sender: TObject); virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseEnter(AControl: TControl); virtual;
    procedure MouseLeave(AControl: TControl); virtual; 
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure Paint; override;
    procedure Resize; override;        
    property State: TcxGizmoState read FState;
    property OnMouseEnter: TNotifyEvent read FOnMouseEnter write FOnMouseEnter;
    property OnMouseLeave: TNotifyEvent read FOnMouseLeave write FOnMouseLeave;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Click; override;
    property AutoCheck: Boolean read FAutoCheck write SetAutoCheck default False;
    property Borders: TcxBorders read FBorders write SetBorders default [bTop, bLeft, bRight, bBottom];
    property CaptionOffset: Integer read FCaptionOffset write SetCaptionOffset default 8;
    property Down: Boolean read FDown write SetDown default False;   
    property Font;
    property ImageAlignment: TcxImageAlignment read FImageAlignment write SetImageAlignment default iaRight;
    property ImageIndex: TImageIndex read FImageIndex write SetImageIndex default -1;
    property ImageOffset: Integer read FImageOffset write SetImageOffset default 8;
    property Images: TCustomImageList read FImageList write SetImageList;
    property Kind: TcxGizmoKind read FKind write SetKind default gkPanel;
    property LookAndFeel: TcxLookAndFeel read FLookAndFeel write SetLookAndFeel;
    property Margin: Integer read FMargin write SetMargin default 0;
    property Margins: TcxMargins read FMargins write SetMargins default [mTop, mLeft, mRight, mBottom];
    property PaintColor: Boolean read FPaintColor write SetPaintColor default False;
    property Painter: TcxOGCustomPainter read GetPainter;
    property PaintFocused: Boolean read FPaintFocused write SetPaintFocused default False;
    property PaintStyle: TcxPaintStyle read GetPaintStyle write SetPaintStyle default psUseLookAndFeel;
    property ParentStyle: Boolean read FParentStyle write SetParentStyle default True;
    property ShowBorder: Boolean read FShowBorder write SetShowBorder default True;
    property ShowCaption: Boolean read FShowCaption write SetShowCaption default False;
    property Transparent: Boolean read FTransparent write SetTransparent default False;
    property CaptionWidth: Integer read GetCaptionWidth;
  end;

  TcxOfficeGizmo = class(TcxCustomOfficeGizmo)
  published
    { Published declarations }
    property Action;
    property Align;
    property Alignment default taLeftJustify;
    property Anchors;
    property AutoCheck;
    property AutoSize;
    property Borders;
    property BorderWidth default 1;
    property Caption;
    property CaptionOffset;
    property Color;
    property Constraints;
    property Down;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property ImageAlignment;
    property ImageIndex;
    property ImageOffset;
    property Images;
    property Kind;
    property LookAndFeel;
    property Margin;
    property Margins;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDblClick;
    property OnDockDrop;
    property OnDockOver;
    property OnDragDrop;
    property OnDragOver;
    property OnEnter;
    property OnExit;
    property OnMouseActivate;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property PaintColor;
    property PaintFocused;
    property PaintStyle;
    property ParentFont;
    property ParentShowHint;
    property ParentStyle;
    property PopupMenu; 
    property ShowBorder;
    property ShowCaption;
    property ShowHint;  
    property TabOrder;
    property TabStop;   
    property Transparent;
    property Visible;
  end;

implementation

uses dxThemeManager, cxOGPainters, Forms, TypInfo;

{ TcxCustomOfficeGizmo }

constructor TcxCustomOfficeGizmo.Create(AOwner: TComponent);
begin
  inherited;
  FPaintFocused := False;
  FAutoCheck := False;
  FDown := False;
  FParentStyle := True;
  DoubleBuffered := True;
  FullRepaint := False;
  ParentBackground := False;
  BevelOuter := bvNone;

  FBorders := [bTop, bLeft, bBottom, bRight];
  FShowBorder := True;
  BorderWidth := 1;

  FMargins := [mTop, mLeft, mBottom, mRight];
  FMargin := 0;

  FKind := gkPanel;
  FPaintStyle := psUseLookAndFeel;
  FLookAndFeel := TcxLookAndFeel.Create(Self);
  FLookAndFeel.OnChanged := LookAndFeelChanged;

  Alignment := taLeftJustify;
  FMouseOver := False;
  FState := gsNormal;

  FImageList := nil;
  FImageChangeLink := TChangeLink.Create;
  FImageChangeLink.OnChange := ImageListChange;
  FImageAlignment := iaRight;
  FImageOffset := 8;
  FImageIndex := -1;

  FShowCaption := False;
  FCaptionOffset := 8;
end;

destructor TcxCustomOfficeGizmo.Destroy;
begin
  if Assigned(FPainter) then
    FreeAndNil(FPainter);
  FreeAndNil(FLookAndFeel); 
  FreeAndNil(FImageChangeLink);
  inherited;
end;         

procedure TcxCustomOfficeGizmo.ActionChange(Sender: TObject;
  CheckDefaults: Boolean);
begin
  inherited ActionChange(Sender, CheckDefaults);
  if Sender is TCustomAction then
    with TCustomAction(Sender) do
    begin
      if not CheckDefaults or (Self.ImageIndex = -1) then
        Self.ImageIndex := ImageIndex;
      if not CheckDefaults or not Self.Down then
        Self.Down := Checked;
    end;
end;

procedure TcxCustomOfficeGizmo.AdjustClientRect(var Rect: TRect);
begin
  if ShowBorder then
  begin
    if bLeft in Borders then
      Inc(Rect.Left, BorderWidth);
    if bRight in Borders then
      Dec(Rect.Right, BorderWidth);
    if bTop in Borders then
      Inc(Rect.Top, BorderWidth);
    if bBottom in Borders then
      Dec(Rect.Bottom, BorderWidth);
  end;

  if mLeft in Margins then
    Inc(Rect.Left, Margin);
  if mRight in Margins then
    Dec(Rect.Right, Margin);
  if mTop in Margins then
    Inc(Rect.Top, Margin);
  if mBottom in Margins then
    Dec(Rect.Bottom, Margin);
end;

procedure TcxCustomOfficeGizmo.Click;
begin
  inherited;

end;

procedure TcxCustomOfficeGizmo.CMEnabledchanged(var Message: TMessage);
begin    
  inherited;
  Invalidate;
end;

procedure TcxCustomOfficeGizmo.CMMouseEnter(var Message: TMessage);
begin
  inherited;
  if Message.lParam = 0 then
    MouseEnter(Self)
  else
    MouseEnter(TControl(Message.lParam));
end;

procedure TcxCustomOfficeGizmo.CMMouseLeave(var Message: TMessage);
begin
  inherited;
  if Message.lParam = 0 then
    MouseLeave(Self)
  else
    MouseLeave(TControl(Message.lParam));
end;

function TcxCustomOfficeGizmo.ControlNeedsInvalidate(const AControl: TControl): Boolean;
var
  I: Integer;
const
  Properties: array[0..2] of string = (
    'Transparent',
    'ParentBackground',
    'NativeBackground'
  );
var
  PropInfo: PPropInfo;
begin
  Result := False;
  for I := Low(Properties) to High(Properties) do
  begin
    PropInfo := GetPropInfo(AControl, Properties[I]);
    if Assigned(PropInfo) and (GetOrdProp(AControl, PropInfo) = 1) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

procedure TcxCustomOfficeGizmo.DblClick;
begin
  if Assigned(OnDblClick) then
    OnDblClick(Self)
  else if Assigned(OnClick) then
    OnClick(Self);
end;

procedure TcxCustomOfficeGizmo.DoImageListChange;
begin

end;

function TcxCustomOfficeGizmo.GetActionLinkClass: TControlActionLinkClass;
begin
  Result := TcxOfficeGizmoActionLink;
end;

function TcxCustomOfficeGizmo.GetActualPaintStyle: TcxPaintStyle;
begin
  Result := PaintStyle;
  if Result = psUseLookAndFeel then
  begin
    if LookAndFeel.NativeStyle and AreVisualStylesAvailable then
      Result := psStandard
    else
      case LookAndFeel.Kind of
        lfFlat, lfUltraFlat: Result := psOffice10;
        lfOffice11: Result := psOffice11;
      else
        Result := psStandard;
      end;
  end;
end;

function TcxCustomOfficeGizmo.GetCaptionWidth: Integer;
var
  Painter: TcxOGCustomPainter;
begin
  Painter := GetPainter;
  Painter.Paint;
  Result := Painter.ParentInfo.Canvas.TextWidth(Caption);
end;

function TcxCustomOfficeGizmo.GetPainter: TcxOGCustomPainter;
var
  AClass: TcxOGPainterClass;
  AStyle: TcxPaintStyle;
begin
  if (PaintStyle = psUseLookAndfeel) and (LookAndFeel.SkinName <> '') then
    AClass := TcxOGSkinPainter
  else
  begin
    AStyle := GetActualPaintStyle;
    case AStyle of
      psOffice10: AClass := TcxOGOffice10Painter;
      psOffice11: AClass := TcxOGOffice11Painter;
    else
      AClass := TcxOGStandardPainter;
    end;
  end;
  if Assigned(FPainter) and not (FPainter is AClass) then
    FreeAndNil(FPainter);
  if not Assigned(FPainter) then
    FPainter := AClass.Create(Self);
  Result := FPainter;
end; 

function TcxCustomOfficeGizmo.GetPaintStyle: TcxPaintStyle;
var
  AControl: TWinControl;
begin
  Result := FPaintStyle;
  if ParentStyle then
  begin
    AControl := Parent;
    while Assigned(AControl) do
    begin
      if AControl is TcxCustomOfficeGizmo then
      begin
        Result := TcxCustomOfficeGizmo(AControl).PaintStyle;
        Exit;
      end;
      AControl := AControl.Parent;
    end;
  end;
end;

procedure TcxCustomOfficeGizmo.ImageListChange(Sender: TObject);
begin
  InvalidateControl(Self);
end;

procedure TcxCustomOfficeGizmo.InvalidateControl(const AControl: TControl;
  const Force: Boolean = False);
var
  I: Integer;
begin
  if not (csLoading in ComponentState) and not (csDestroying in ComponentState) then
  begin
    AControl.Invalidate;
    if AControl is TWinControl then
    begin
      for I := 0 to TWinControl(AControl).ControlCount - 1 do
      begin
        if Force or ControlNeedsInvalidate(TWinControl(AControl).Controls[I]) then
          InvalidateControl(TWinControl(AControl).Controls[I], Force);
      end;
    end;
  end;
end;

procedure TcxCustomOfficeGizmo.LookAndFeelChanged(Sender: TcxLookAndFeel;
  AChangedValues: TcxLookAndFeelValues);
begin
  if not (csDestroying in ComponentState) then
    InvalidateControl(Self);
end;

procedure TcxCustomOfficeGizmo.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FState := gsPressed;
  InvalidateControl(Self);
end;

procedure TcxCustomOfficeGizmo.MouseEnter(AControl: TControl);
begin
  if Assigned(FOnMouseEnter) then FOnMouseEnter(Self);  
  if (csDesigning in ComponentState) then
    Exit;
  FMouseOver := True;
  FState := gsHot;    
  if (Kind = gkButton) or (Kind = gk3DButton) then
    InvalidateControl(Self);
end;

procedure TcxCustomOfficeGizmo.MouseLeave(AControl: TControl);
begin
  if Assigned(FOnMouseLeave) then FOnMouseLeave(Self); 
  FMouseOver := False;
  FState := gsNormal;
  if (Kind = gkButton) or (Kind = gk3DButton) then
    InvalidateControl(Self);
end;         

procedure TcxCustomOfficeGizmo.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  if FMouseOver then
    FState := gsHot
  else
    FState := gsNormal;
  if AutoCheck then
    Down := not Down;
  InvalidateControl(Self);
end;

procedure TcxCustomOfficeGizmo.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
    if AComponent = Images then
      Images := nil;
end;    

procedure DrawParentImage(const AControl: TControl; const DC: HDC);
var
  SaveIndex: Integer;
  P: TPoint;
begin
  if AControl.Parent = nil then
    Exit;

  SaveIndex := SaveDC(DC);
  try
    GetViewportOrgEx(DC, P);

    SetViewportOrgEx(DC, P.X - AControl.Left, P.Y - AControl.Top, nil);
    IntersectClipRect(DC, 0, 0, AControl.Parent.ClientWidth,
      AControl.Parent.ClientHeight);

    AControl.Parent.Perform(WM_ERASEBKGND, Longint(DC), 0);
    AControl.Parent.Perform(WM_PAINT, Longint(DC), 0);
  finally
    RestoreDC(DC, SaveIndex);
  end;

  if not (AControl.Parent is TCustomControl) and
    not (AControl.Parent is TCustomForm) then
    AControl.Parent.Invalidate;
end;

procedure TcxCustomOfficeGizmo.Paint;
begin
  //don't call inherited, we're doing all the painting ourselves
  if (Transparent or (LookAndFeel.SkinName <> '')) and not (csDesigning in ComponentState) then
    DrawParentImage(Self, Canvas.Handle);
  Painter.Paint;
end;

procedure TcxCustomOfficeGizmo.Resize;
begin
  inherited;
  InvalidateControl(Self);
end;

procedure TcxCustomOfficeGizmo.SetAutoCheck(const Value: Boolean);
begin
  FAutoCheck := Value;
end;

procedure TcxCustomOfficeGizmo.SetBorders(const Value: TcxBorders);
begin
  if FBorders <> Value then
  begin
    FBorders := Value;        
    Realign;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetCaptionOffset(const Value: Integer);
var
  AValue: Integer;
begin       
  AValue := Value;
  if Value < 0 then
    AValue := 0;
  if AValue <> FCaptionOffset then
  begin
    FCaptionOffset := AValue;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetDown(const Value: Boolean);
begin  
  if Value <> FDown then
  begin
    FDown := Value;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetImageAlignment(
  const Value: TcxImageAlignment);
begin
  if Value <> FImageAlignment then
  begin
    FImageAlignment := Value;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetImageIndex(const Value: TImageIndex);
begin
  if FImageIndex <> Value then
  begin
    FImageIndex := Value;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetImageList(const Value: TCustomImageList);
begin                
  if FImageList <> Value then
  begin
    if Images <> nil then
      Images.UnRegisterChanges(FImageChangeLink);
    FImageList := Value;
    DoImageListChange;
    if Images <> nil then
    begin
      FImageList.RegisterChanges(FImageChangeLink);
      FImageList.FreeNotification(Self);
    end;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetImageOffset(const Value: Integer);
var
  AValue: Integer;
begin
  AValue := Value;
  if Value < 0 then
    AValue := 0;
  if AValue <> FImageOffset then
  begin
    FImageOffset := AValue;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetKind(const Value: TcxGizmoKind);
begin
  if FKind <> Value then
  begin
    FKind := Value;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetLookAndFeel(const Value: TcxLookAndFeel);
begin
  FLookAndFeel.Assign(Value);
end;

procedure TcxCustomOfficeGizmo.SetMargin(const Value: Integer);
begin
  if FMargin <> Value then
  begin
    FMargin := Value;
    Realign;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetMargins(const Value: TcxMargins);
begin                 
  if FMargins <> Value then
  begin
    FMargins := Value;
    Realign;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetPaintColor(const Value: Boolean);
begin     
  if FPaintColor <> Value then
  begin
    FPaintColor := Value;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetPaintFocused(const Value: Boolean);
begin       
  if Value <> FPaintFocused then
  begin
    FPaintFocused := Value;
    InvalidateControl(Self);  
  end;
end;

procedure TcxCustomOfficeGizmo.SetPaintStyle(const Value: TcxPaintStyle);
begin
  if Value <> FPaintStyle then
  begin
    FPaintStyle := Value;
    InvalidateControl(Self, True);
  end;
end;

procedure TcxCustomOfficeGizmo.SetParentStyle(const Value: Boolean);
begin
  FParentStyle := Value;
end;

procedure TcxCustomOfficeGizmo.SetShowBorder(const Value: Boolean);
begin
  if FShowBorder <> Value then
  begin
    FShowBorder := Value;
    Realign;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetShowCaption(const Value: Boolean);
begin
  if FShowCaption <> Value then
  begin
    FShowCaption := Value;
    InvalidateControl(Self);
  end;
end;

procedure TcxCustomOfficeGizmo.SetTransparent(const Value: Boolean);
begin
  if FTransparent <> Value then
  begin
    FTransparent := Value;
    if FTransparent and not (csDesigning in ComponentState) then
      ControlStyle := ControlStyle - [csOpaque]
    else
      ControlStyle := ControlStyle + [csOpaque];
    InvalidateControl(Self);
  end;
end;

{ TcxOGCustomPainter }

constructor TcxOGCustomPainter.Create(AParent: TcxCustomOfficeGizmo);
begin                                 
  inherited Create;
  FParentControl := AParent;
  FParentInfo := TcxOGPainterParentInfo.Create(AParent);
end;

destructor TcxOGCustomPainter.Destroy;
begin
  FreeAndNil(FParentInfo);
  inherited;
end;

{ TcxOGPainterParentInfo }

constructor TcxOGPainterParentInfo.Create(AParent: TcxCustomOfficeGizmo);
begin
  inherited Create;
  FCanvas := TcxCanvas.Create(AParent.Canvas);
  FParentControl := AParent;
end;

destructor TcxOGPainterParentInfo.Destroy;
begin
  FreeAndNil(FCanvas);
  inherited;
end;

function TcxOGPainterParentInfo.GetAlignment: TAlignment;
begin
  Result := ParentControl.Alignment;
end;

function TcxOGPainterParentInfo.GetBorders: TcxBorders;
begin
  Result := ParentControl.Borders;
end;

function TcxOGPainterParentInfo.GetBorderWidth: Integer;
begin
  Result := ParentControl.BorderWidth;
end;

function TcxOGPainterParentInfo.GetCanvas: TcxCanvas;
begin
  Result := FCanvas;
end;

function TcxOGPainterParentInfo.GetCaption: string;
begin
  Result := ParentControl.Caption;
end;

function TcxOGPainterParentInfo.GetCaptionOffset: Integer;
begin
  Result := ParentControl.CaptionOffset;
end;

function TcxOGPainterParentInfo.GetClientRect: TRect;
begin
  Result := ParentControl.ClientRect;
end;

function TcxOGPainterParentInfo.GetColor: TColor;
begin
  Result := ParentControl.Color;
end;

function TcxOGPainterParentInfo.GetDown: Boolean;
begin
  Result := ParentControl.Down;
end;

function TcxOGPainterParentInfo.GetEnabled: Boolean;
begin
  Result := ParentControl.Enabled;
end;

function TcxOGPainterParentInfo.GetHandle: HWND;
begin
  Result := ParentControl.Handle;
end;

function TcxOGPainterParentInfo.GetHeight: Integer;
begin
  Result := ParentControl.Height;
end;

function TcxOGPainterParentInfo.GetImageAlignment: TcxImageAlignment;
begin
  Result := ParentControl.ImageAlignment;
end;

function TcxOGPainterParentInfo.GetImageIndex: TImageIndex;
begin
  Result := ParentControl.ImageIndex;
end;

function TcxOGPainterParentInfo.GetImageOffset: Integer;
begin
  Result := ParentControl.ImageOffset;
end;

function TcxOGPainterParentInfo.GetImages: TCustomImageList;
begin
  Result := ParentControl.Images;
end;

function TcxOGPainterParentInfo.GetKind: TcxGizmoKind;
begin
  Result := ParentControl.Kind;
end;

function TcxOGPainterParentInfo.GetMargin: Integer;
begin
  Result := ParentControl.Margin;
end;

function TcxOGPainterParentInfo.GetMargins: TcxMargins;
begin
  Result := ParentControl.Margins;
end;

function TcxOGPainterParentInfo.GetPaintColor: Boolean;
begin
  Result := ParentControl.PaintColor;
end;

function TcxOGPainterParentInfo.GetPaintFocused: Boolean;
begin
  Result := ParentControl.PaintFocused;
end;

function TcxOGPainterParentInfo.GetShowBorder: Boolean;
begin
  Result := ParentControl.ShowBorder;
end;

function TcxOGPainterParentInfo.GetShowCaption: Boolean;
begin
  Result := ParentControl.ShowCaption;
end;

function TcxOGPainterParentInfo.GetState: TcxGizmoState;
begin
  Result := ParentControl.State;
end;

function TcxOGPainterParentInfo.GetTransparent: Boolean;
begin
  Result := ParentControl.Transparent;
end;

function TcxOGPainterParentInfo.GetWidth: Integer;
begin
  Result := ParentControl.Width;
end;

{ TcxOfficeGizmoActionLink }

procedure TcxOfficeGizmoActionLink.AssignClient(AClient: TObject);
begin
  inherited;
  FClient := AClient as TcxCustomOfficeGizmo;
end;

function TcxOfficeGizmoActionLink.IsCheckedLinked: Boolean;
begin
  Result := inherited IsCheckedLinked and (FClient is TcxCustomOfficeGizmo) and
    (TcxCustomOfficeGizmo(FClient).Down = (Action as TCustomAction).Checked);
end;

function TcxOfficeGizmoActionLink.IsImageIndexLinked: Boolean;
begin
  Result := inherited IsImageIndexLinked and
    (FClient.ImageIndex = (Action as TCustomAction).ImageIndex);
end;

procedure TcxOfficeGizmoActionLink.SetChecked(Value: Boolean);
begin
  if IsCheckedLinked and (FClient is TcxCustomOfficeGizmo) then
    TcxCustomOfficeGizmo(FClient).Down := Value;
end;

procedure TcxOfficeGizmoActionLink.SetImageIndex(Value: Integer);
begin
  if IsImageIndexLinked then FClient.ImageIndex := Value;
end;

end.
